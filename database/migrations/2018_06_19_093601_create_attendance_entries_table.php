<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_entries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id');
            $table->string('emp_name');
            $table->integer('emp_code');
            $table->dateTime('in_time')->nullable();
            $table->dateTime('out_time')->nullable();
            $table->double('late_in')->nullable();
            $table->double('early_out')->nullable();
            $table->double('extra')->nullable();
            $table->double('meal_amount')->nullable();
            $table->time('shift_timing');
            $table->string('remark')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('attendance_status')->nullable();
            $table->tinyInteger('verification_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_entries');
    }
}
