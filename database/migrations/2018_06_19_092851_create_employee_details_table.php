<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dept_id');
            $table->integer('company_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('mobile');
            $table->bigInteger('emp_code');
            $table->date('dob');
            $table->date('doj');
            $table->smallInteger('relationship');
            $table->string('relative_name');
            $table->longText('address1');
            $table->longText('address2');
            $table->string('emp_type');
            $table->string('designation');
            $table->string('marital_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_details');
    }
}
