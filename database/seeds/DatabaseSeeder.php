<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CompanySeeder::class);
        $this->call(DepartmentSeeder::class);
        // $this->call(EmployeeSeeder::class);
        $this->call(ShiftTimingsSeeder::class);
        $this->call(HolidaySeeder::class);


    }
}
