<?php

use App\Department;
use App\EmployeeDetails;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Company;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        EmployeeDetails::truncate();

        $faker = Faker::create('en_IN');
        $department = Department::all()->pluck('id');
        $company = Company::all()->pluck('id');


        foreach (range(0,200) as $count){

            $randomCompany = $faker->randomElement($company);
            EmployeeDetails::create([
                'company_id' => $randomCompany,
                'dept_id' => $faker->randomElement(Department::where('company_id', $randomCompany)->pluck('id')),
                'first_name' => $faker->name,
                'last_name' => $faker->name,
                'mobile' => $faker->mobileNumber,
                'doj' => $faker->date($format = 'Y-m-d', $max = '2018-07-01'),
                'designation' => $faker->jobTitle,
                'dob' => $faker->date($format = 'Y-m-d', $max = '2000-01-01'),
                'emp_code' => $faker->randomNumber(3),
                'relationship' => $faker->randomElement([1, 2, 3]),
                'relative_name' => $faker->name,
                'address1' => $faker->streetAddress.', '. $faker->city.', '.$faker->postcode.', '. $faker->state,
                'address2' => $faker->streetAddress.', '. $faker->city.', '.$faker->postcode.', '. $faker->state,
                'emp_type' => $faker->randomElement(['regular','contract']),
            ]);
        }
    }
}
