<?php

use App\ShiftInfo;
use Illuminate\Database\Seeder;

class ShiftTimingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ShiftInfo::truncate();

        $shiftInfos = [
            ['SCPL - I', '07:00:00', '15:00:00','1'],
            ['SCPL - II', '15:00:00', '23:00:00','1'],
            ['NSCPL', '23:00:00', '08:00:00','1'],
            ['SCPL - G1', '08:00:00', '16:00:00','1'],
            ['SCPL - G2', '09:00:00', '17:00:00','1'],
            ['SCPL - G3', '09:30:00', '17:30:00','1'],
            ['SCPL - G4', '10:00:00', '18:00:00','1'],
            ['NSCPL - N', '20:00:00', '06:00:00','1'],
            ['NSCPL - SEVEN', '19:00:00', '04:00:00','1'],
            ['SCPL - ZERO', '00:00:00', '00:00:00','1'],
            ['GD - I', '08:00:00', '16:00:00','2'],
            ['GI - I', '08:00:00', '16:00:00','3'],
            ['GD - II', '16:00:00', '00:00:00','2'],
            ['GI - II', '16:00:00', '00:00:00','3'],
            ['GD - N', '00:00:00', '08:00:00','2'],
            ['GI - N', '00:00:00', '08:00:00','3'],
            ['GD - G1', '09:30:00', '17:30:00','2'],
            ['GI - G1', '09:30:00', '17:30:00','3'],
            ['GD - G2', '10:00:00', '18:00:00','2'],
            ['GI - G2', '10:00:00', '18:00:00','3'],
            ['GD - ZERO', '00:00:00', '00:00:00','2'],
            ['GI - ZERO', '00:00:00', '00:00:00','3'],
            ['GI - NIGHT', '00:00:01', '00:00:01','2'],

        ];

        foreach ($shiftInfos as $shiftInfo){

            ShiftInfo::create([
                'shift' => $shiftInfo[0],
                'in_time' => $shiftInfo[1],
                'out_time' => $shiftInfo[2],
                'company_id'=>$shiftInfo[3]
            ]);
        }
    }
}
