<?php

use App\Company;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Company::truncate();
        $companies = ['Shakti Cords Pvt. Ltd.', 'GD Textiles Pvt. Ltd', 'Ganapathy Textiles Pvt. Ltd'];

        foreach ($companies as $company){
            Company::create([
                'name' => $company,
            ]);
        }
    }
}
