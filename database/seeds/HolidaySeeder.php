<?php

use App\HolidayDetail;
use Illuminate\Database\Seeder;

class HolidaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $holidays = [
            ['New year','2018-01-01'],
            ['Republic Day', '2018-01-26'],
            ['Pongal', '2018-01-14'],
            ['Good Friday', '2018-03-30'],
            ['May Day', '2018-05-01'],
            ['Independance Day', '2018-08-15'],
            ['Gandhi jeyanthi', '2018-10-02'],
            ['Vijaya Dasami', '2018-10-19'],
            ['Deepavali', '2018-11-06'],
            ['Christmas', '2018-12-25']
        ];


        foreach ($holidays as $holiday){
            HolidayDetail::create([
                'description' => $holiday[0],
                'date' => $holiday[1]
            ]);
        }
    }
}
