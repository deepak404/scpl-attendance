<?php

use App\Department;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//
//        $faker = Faker::create();
//
//        foreach (range(1, 5) as $index){
//            User::create([
//                'name' => $faker->name,
//                'email'=>$faker->email,
//                'password'=> bcrypt('123456'),
//                'role' => $faker->randomElement(['hod','admin'])
//            ]);
//        }

        Department::truncate();

        $shaktiDepartments = ['Auditor','Dipping' ,'Electrical', 'Executies', 'Final Winding', 'Karthick Con', 'Lab', 'Mechanical', 'Office', 'Packing',
            'Personnel', 'Sales', 'Security', 'Shift Production Engg.', 'Store', 'Twisting'];

        $gdDepartments = ['Auditor','Dipping' ,'Electrical', 'Executies', 'Final Winding', 'Karthick Con', 'Lab', 'Mechanical', 'Office', 'Packing',
            'Personnel', 'Sales', 'Security', 'Shift Production Engg.', 'Store', 'Twisting'];

        $ganapathyDepartments = ['Auditor','Dipping' ,'Electrical', 'Executies', 'Final Winding', 'Karthick Con', 'Lab', 'Mechanical', 'Office', 'Packing',
            'Personnel', 'Sales', 'Security', 'Shift Production Engg.', 'Store', 'Twisting'];

        foreach ($shaktiDepartments as $department){

//            dd($department);
            Department::create([
                'company_id' => 1,
                'name' => $department,
            ]);
        }


        foreach ($gdDepartments as $department){

//            dd($department);
            Department::create([
                'company_id' => 2,
                'name' => $department,
            ]);
        }

        foreach ($ganapathyDepartments as $department){

//            dd($department);
            Department::create([
                'company_id' => 3,
                'name' => $department,
            ]);
        }



    }
}
