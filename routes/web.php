<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    if(\Auth::user()){
        if(\Auth::user()->role == 'admin'){
            return redirect('/home');
        }else if(\Auth::user()->role == 'hod'){
            return redirect('/daily_report');
        }
    }

    return view('auth.login');



});

Route::get('/scpl/{key}','masterController@index');

Route::get('/exportEmp', 'HomeController@employeeExport');

Auth::routes();


Route::group(['middleware' => ['admin','auth']], function () {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/employee', 'HomeController@employee')->name('employee');

    Route::post('/add_emp','HomeController@addEmployee');

    Route::post('/add_bank','HomeController@addBank');

    Route::post('/add_edu','HomeController@addEducation');

    Route::post('/add_ped','HomeController@addPreviousExperienceDetails');

    Route::post('/add_family','HomeController@addFamily');

    Route::post('/add_reference','HomeController@addReference');

    Route::post('/add_document','HomeController@addDocument');

    Route::get('/find','HomeController@findEmployee');

    Route::get('/find_employees','HomeController@findAllEmployees');

    Route::post('/get_emp','HomeController@getEmployee');

    Route::post('/delete_emp','HomeController@deleteEmployee');

    Route::post('/csv_upload','HomeController@csvUpload');

    Route::get('/report_details','HomeController@getreportDetails')->name('report_details');

    Route::post('/get_daily_report','HomeController@getDailyReport');

    Route::post('/get_confirmation_report','HomeController@getConfirmationReport');

    Route::get('/weekly_off','HomeController@populateWeeklyOff');

    Route::get('/report_list','HomeController@showReportList')->name('report_list');

    Route::post('/generate_report','HomeController@generateReport');

    Route::get('/dummy_view','HomeController@dummyView');

    Route::get('/confirmation_report_admin/{id?}', 'HomeController@confirmationReport')->name('confirmation_report_admin');

    Route::get('/daily_report_admin/{id?}', 'HomeController@dailyReport')->name('daily_report_admin');

    Route::get('/manual_entry', 'HomeController@manualEntry')->name('manual');

    Route::post('/get_manual', 'HomeController@getManual');

    Route::post('/delete_entry', 'HomeController@deleteEntry');

    Route::post('/update_manual', 'HomeController@updateManual');
});


Route::group(['middleware' => ['hod','auth']], function () {

    Route::get('/confirmation_report', 'DepartmentController@getConfirmationReport')->name('hod_confirmation_report');

    Route::get('/daily_report', 'DepartmentController@getDailyReport')->name('hod_daily_report');

});

Route::group(['middleware' => 'auth'], function() {

    Route::post('/update_confirmation', 'DepartmentController@updateConfirmation');

    Route::post('/approve_all', 'DepartmentController@approveAll');

    Route::post('/get_by_date', 'DepartmentController@getByDate');

    Route::post('/update_daily', 'DepartmentController@updateDaily');

    Route::post('/get_by_day', 'DepartmentController@getByDay');

});


Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

// ******************Test Run*************************

// Route::get('empUpdate','TestController@empUpdate');

// Route::get('weeklyOff', 'TestController@weekOfupdate');

// Route::get('checkAttendance', 'TestController@checkAttendance');

// Route::get('groupEmp', 'TestController@groupByEmpType');
