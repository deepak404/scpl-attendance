<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SCPL | Employee</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <!-- Optional theme -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"> -->
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />


    <link rel="stylesheet" href="{{url('css/global.css?v=0.1')}}">
    <link rel="stylesheet" href="{{url('css/employee.css?v=0.1')}}">
    <link rel="stylesheet" href="{{url('css/jquery-ui.css?v=0.1')}}">
    <style media="screen">
      .row{
        padding: 40px;
      }
      h1{
        margin-left: 20px;
        margin-top: 30px;
      }
      h2{
        margin-left: 30px;
      }
      .save-btn{
        margin-left: 0px;
      }
      .twitter-typeahead{
        position: relative;
        display: inline-block;
        width: 100%;
        padding: 0px;
        margin: 0px;
      }
      #search-id{
        margin: 0px;
        border: 1px solid #ccc;
      }
      .modal-title{
        background-color: white;
      }
      .attendance-table{
        width: 100%;
      }
      #attendance-tbody>tr>td{
        padding: 10px;
      }
      .last{
        text-align: center;
      }
      tr{
        border-bottom: 1px solid #d3d3d3;
      }
    </style>

</head>
<body>
<div class="wrapper container">
    <!-- Sidebar -->
@include('layouts/admin')
<!-- Page Content -->
    <main class="main">
      <h1>Get Employee</h1>
      <!-- Hello -->
      <div class="row">
        <form class="col-md-12" id="get_emp_form">
          <input type="hidden" name="id" id="emp_id" value="">
          <div class="form-group col-md-6">
              <label>Employee Name</label>
              <input class="form-control" name="employee-name" id="search-id" autocomplete="off">
          </div>
          <div class="form-group col-md-6">
            <label>Date</label>
            <input class="form-control" type="text" name="date" id="select-date" autocomplete="off">
          </div>
          <div class="form-group col-md-12">
            <input class="btn btn-primary save-btn" type="submit" value="Get">
          </div>
        </form>
      </div>
      <h2>Employee Attendance Entry</h2>
      <div class="row">
        <form class="col-md-12" id="emp_attendance_entry">
          <input type="hidden" name="emp_id" id="emp-id" value="">
          <input type="hidden" name="entry_id" id="entry_id" value="">
          <div class="form-group col-md-6">
            <label>Employee Name</label>
            <input class="form-control" type="text" name="emp_name" id="emp-name" readonly value="">
          </div>
          <div class="form-group col-md-6">
            <label>Company</label>
            <input class="form-control" type="text" name="company" id="company" readonly value="">
          </div>
          <div class="form-group col-md-6">
            <label>In Time</label>
            <input class="form-control" type="text" name="in_time" id="in-time" value="" required autocomplete="off">
          </div>
          <div class="form-group col-md-6">
            <label>Out Time</label>
            <input class="form-control" type="text" name="out_time" id="out-time" value="" autocomplete="off">
          </div>
          <div class="form-group col-md-6">
            <label>Late In</label>
            <input class="form-control" type="text" name="late_in" id="late-in" value="">
          </div>
          <div class="form-group col-md-6">
            <label>Early Out</label>
            <input class="form-control" type="text" name="early_out" id="early-out" value="">
          </div>
          <div class="form-group col-md-4">
            <label>Shift Time</label>
            <select class="form-control" name="shift_timing" id="shift-timing">

            </select>
          </div>
          <div class="form-group col-md-4">
            <label>Attendance Status</label>
            <select class="form-control" name="attendance_status" id="attendance-status">
              <option value=""></option>
              <option value="P">P</option>
              <option value="D">D</option>
              <option value="H">H</option>
              <option value="L">L</option>
              <option value="A">A</option>
              <option value="O">O</option>
              <option value="V">V</option>
              <option value="W">W</option>
              <option value="X">X</option>
              <option value="Y">Y</option>
              <option value="Z">Z</option>
            </select>
          </div>
          <div class="form-group col-md-4">
            <label>Entry Status</label>
            <select class="form-control" name="status" id="status">
              <option value="0">open</option>
              <option value="1">close</option>
            </select>
          </div>
          <div class="form-group col-md-12">
            <input class="btn btn-primary save-btn" type="submit" name="Update" value="Update">
          </div>
        </form>
      </div>
    </main>
</div>
<!-- Modal -->
<div id="attendance-list" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Attendance list</h4>
      </div>
      <div class="modal-body">
        <table class="attendance-table">
          <thead>
            <tr>
              <th>Employee</th>
              <th>In Time</th>
              <th>Out Time</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody id="attendance-tbody">

          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="refresh">Refresh</button>
      </div>
    </div>

  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>
<script src="{{url('js/manual_entry.js')}}"></script>
<script type="text/javascript" src="js/smartsearch.js?v=0.1"></script>

</body>
</html>
