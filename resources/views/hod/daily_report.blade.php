<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SCPL | Employee</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="css/global.css">
    <link rel="stylesheet" href="css/employee.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
</head>
<body>
<div class="wrapper container">
    <!-- Sidebar -->
@include('layouts/dept')
<!-- Page Content -->
    <main>
        <div class= row>
            <div class="col-md-12">
                <h2>Daily Report</h2>
                <p id="filter-holder">
                    <input type="text" id="date-selector" name="date-selector" class="form-control" value="{{$date}}" placeholder="Date">
                    <button type="button" class="btn btn-primary pull-right" data-date="{{$date}}" id="apprive-btn">Approve All</button>
                </p>

                <div class="table-wrapper">
                    <table class="table table-bordered dept-daily-report-table">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Employee Name</th>
                                <th>Shift</th>
                                <th>In</th>
                                <th>Late</th>
                                <th>Verified</th>
                            </tr>
                        </thead>
                        <tbody id="daily-report-tboby">
                            <?php $count = 1; ?>
                            @foreach($attendance as $entry)
                                <tr data-entryid = "{{$entry['id']}}">
                                    <td>{{$count}}.</td>
                                    <td>{{$entry['emp_name']}}</td>
                                    <td>
                                        <select name="shift-name" id="shift-name{{$entry['id']}}" >
                                            @foreach($shifts as $value)
                                                @if($value['in_time'] == $entry['shift_timing'])
                                                <option value="{{$value['in_time']}}" selected>{{$value['in_time']}}</option>
                                                @else
                                                <option value="{{$value['in_time']}}">{{$value['in_time']}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>{{date('H:i:s',strtotime($entry['in_time']))}}</td>
                                    <td>{{$entry['late_in']}}</td>
                                    <td>
                                        <input type="text" id="remarks{{$entry['id']}}" value="{{$entry['remark']}}" placeholder="Remarks">
                                        <button type="button" class="btn btn-primary update-btn" data-entryid = "{{$entry['id']}}">Update</button>
                                    </td>
                                </tr>
                                <?php $count += 1; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
</div>
<div id="response-modal" class="modal fade">
    <div class="modal-dialog" id="save-div">
        <div class="modal-content">
            <div class="modal-body">
                <p class="text-center info-text" id="save-p">Your data has been saved Successfully.</p>
            </div>
            <button class="btn btn-primary center-block" id="save-btn" data-dismiss="modal">Okay</button>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="js/hod/daily_report.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>
</body>
</html>