<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SCPL | Employee</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="css/global.css">
    {{--<link rel="stylesheet" href="css/employee.css">--}}
    <link rel="stylesheet" href="css/jquery-ui.css">
</head>
<style>


    #filter-report-form{
        margin-left: auto;
        margin-right: auto;
        display: block;
        width: 400px;
        padding-top: 20px;
        padding-bottom: 20px;
        box-shadow: 0px 0px 10px #e8e8e8;
        padding-left: 20px;
    }

    #start_date, #end_date, #date, #ind_start_date, #ind_end_date{
        width: 30%;
        display: inline-block;
        margin-right: 10px;
    }

    #employee_name{
        width: 100%;
    }
</style>
<body>
<div class="wrapper">
    <!-- Sidebar -->
@include('layouts/admin')
<!-- Page Content -->
    <div id="content" class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Report</h2>



                <form name="filter-report-form" id="filter-report-form" action="generate_report" method="POST" target="_blank">
                    {{csrf_field()}}
                    <div class="form-group">
                        <p>Report Type: </p>
                        <select name="report-type" id="report-type" required>
                            <option value="cr">Confirmation Report</option>
                            <option value="dr">Daily Report</option>
                            <option value="ia">Individual Attendance</option>
                            <option value="ar">Attendance Register</option>
                            <option value="ma">Monthly Attendance</option>
                            <option value="asone">Attendance Summary 1</option>
                            <option value="astwo">Attendance Summary 2</option>

                        </select>
                    </div>


                    <div class="form-group" style="display: none;" id="daily_report_holder">
                        <p>Date : </p>
                        <input type="text" id="date" name="date" placeholder="Date" class="form-control" required disabled>
                    </div>

                    <div class="form-group" style="display: none;" id="attendance_summary_holder">
                        <p>Date Range : </p>
                        <input type="text" id="start_date" name="start_date" placeholder="Start date" class="form-control" required disabled>
                        <input type="text" id="end_date" name="end_date" class="form-control" placeholder="End Date" required disabled>

                    </div>

                    <div class="form-group" style="display: none;" id="ind_date_holder">
                        <p>Date Range : </p>
                        <input type="text" id="ind_start_date" name="start_date" placeholder="Start date" class="form-control" autocomplete="newstartdate" required disabled>
                        <input type="text" id="ind_end_date" name="end_date" class="form-control" placeholder="End Date" autocomplete="newenddate" required disabled>

                    </div>

                    <div class="form-group" style="display: none;" id="employee_search_holder">
                        <p>Enter Employee Name : </p>
                        <input type="text" id="employee_name" name="employee_name" placeholder="Employee Name" class="form-control" autocomplete="new-search-user" required disabled>

                    </div>


                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach
                    @endif

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" id="report-submit-btn" value="Get">

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="response-modal" class="modal fade">
    <div class="modal-dialog" id="save-div">
        <div class="modal-content">
            <div class="modal-body">
                <p class="text-center info-text" id="save-p">Your data has been saved Successfully.</p>
            </div>
            <button class="btn btn-primary center-block" id="save-btn" data-dismiss="modal">Okey</button>
        </div>
    </div>
</div>
<script src="js/employee.js?v=0.1"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $(document).ready(function(){
        $('#report-type').on('change',function(){
            console.log($(this).val());

            if($(this).val() == "cr"){

                $(this).closest('form').find('.form-group:nth-child(2)').show().find('input').attr('disabled',true);
                $('#employee_search_holder').hide().find('input').attr('disabled',true);
                $('#daily_report_holder, #attendance_summary_holder, #ind_date_holder').hide().find('input').attr('disabled', true);
                $("#report-submit-btn").show();


            }else if($(this).val() == 'dr'){
                $('#daily_report_holder').show().find('input').attr('disabled', false);
                $('#employee_search_holder , #attendance_summary_holder, #ind_date_holder').hide().find('input').attr('disabled',true);
                $("#report-submit-btn").show();


            }
            else if($(this).val() == "asone"){
                $(this).closest('form').find('.form-group:nth-child(2)').show().find('input').attr('disabled',false);
                $('#employee_search_holder ,#daily_report_holder, #ind_date_holder').hide().find('input').attr('disabled',true);
                $('#attendance_summary_holder').show().find('input').attr('disabled', false);
                $("#report-submit-btn").show();


            }else if($(this).val() == "ia"){
                $('#employee_search_holder, #ind_date_holder').find('input').attr('disabled',false);
                $('#employee_search_holder, #ind_date_holder').show();
                $('#attendance_summary_holder, #daily_report_holder').hide().find('input').attr('disabled', true);
                $("#report-submit-btn").hide();

            }
            else{
                $('#employee_search_holder').hide().find('input').attr('disabled', true);
                $(this).closest('form').find('.form-group:nth-child(2)').show().find('input').attr('disabled',true);

            }

        });


        $('#start_date, #ind_start_date').datepicker({ dateFormat: 'dd-mm-yy' });
        $('#end_date, #date, #ind_end_date').datepicker({maxDate: '0',dateFormat: 'dd-mm-yy' });

        var emp_name;
        $(document).on('click', ".suggested-user", function(){

            //Sending Employee ID instead of Employee Name
            $('#employee_name').val($(this).data('id'));
            $('#employee_name').attr('data-empid', $(this).data('id'));

            emp_name = $(this).find('.suggested-name').text();
            console.log($('#employee_name').val());


            $(this).closest('form').submit();
        });



    });
</script>
<script type="text/javascript" src="js/smartsearch_employees.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>
</body>
</html>