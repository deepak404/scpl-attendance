<html>
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>SCPL | Employee</title>
		<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<link rel="stylesheet" href="css/global.css">
	<link rel="stylesheet" href="css/employee.css">
	<link rel="stylesheet" href="css/jquery-ui.css">
</head>
<body>
	<div class="wrapper">
	    <!-- Sidebar -->
		@include('layouts/admin')
	    <!-- Page Content -->
	    <div id="content">
    		<h2 class="col-lg-12">Employee Details<span class="options"><i class="material-icons" id="delete-btn">delete</i></span><input type="text" placeholder="Search.." id="search-id"></h2>
	    	<div class="col-lg-12 master">
	    		<div class="col-lg-12 p-lr-0">
		    		<h4>General Info <div class="loader" id="general-loder"></div></h4>
		    		<form action="POST" class="col-lg-12 col-md-12 p-lr-0" id="empInfo">
						<div class="col-md-12" style="margin-bottom: 10px;">
							<label for="company">Company</label>
							<select name="company" class="form-control" id="company-id">
								<option value="1">Shakti Cords Pvt. Ltd.</option>
								<option value="2">GD Textiles Pvt. Ltd</option>
								<option value="3">Ganapathy Textiles Pvt. Ltd</option>
							</select>
							<!-- <div class="col-md-12 p-lr-0" id="company-div">
							    <label class="radio-inline">
							      <input type="radio" class="company-id" name="company" value="1">SHAKTI
							    </label>
							    <label class="radio-inline">
							      <input type="radio" class="company-id" name="company" value="2">GD Textail
							    </label>
							    <label class="radio-inline">
							      <input type="radio" class="company-id" name="company" value="3">GANAPATHI
							    </label>
							</div> -->
						</div>

		    			<div class="form-group col-lg-6 col-md-12">
					      <label for="firstname">FirstName:</label>
					      <input type="text" class="form-control" id="firstname" placeholder="Enter FirstName" name="firstname" required="true" style="text-transform:uppercase">
		    			</div>
		    			<div class="form-group col-lg-6 col-md-12">
					      <label for="lastname">LastName:</label>
					      <input type="text" class="form-control" id="lastname" placeholder="Enter LastName" name="lastname" required="true" style="text-transform:uppercase">
		    			</div>
		    			<div class="form-group col-lg-6 col-md-12">
					      <label for="dob">Data of Birth:</label>
					      <input type="text" class="form-control" id="dob" placeholder="Data of Birth" name="dob" autocomplete="off" required="true">
		    			</div>
		    			<div class="form-group col-lg-6 col-md-12">
					      <label for="empcode">Employee Code: <span style="font-size: 12px; font-weight: normal;">{{$missing}}</span></label>
					      <input type="number" class="form-control" id="empcode" placeholder="Enter Employee Code" name="empcode" value="{{$empCode}}" required="true">
		    			</div>
		    			<div class="form-group col-lg-6 col-md-12">
					      <div class="col-lg-2 p-lr-0">
						      <label for="relation">Relationsnip:</label>
						      <select name="relation" class="form-control" id="relation">
						      	<option value="1">Father</option>
						      	<option value="2">Husband</option>
						      	<option value="3">Guardian</option>
						      </select>
					      </div>
					      <div class="col-lg-10 p-r-0">
						      <label for="rname">Name:</label>
						      <input type="text" class="form-control" id="rname" placeholder="Enter Name" name="rname" required="true">
					      </div>
						</div>
						<div class="form-group col-lg-6 col-md-12">
					      <label for="emptype">Employee Type:</label>
					      <select name="emptype" class="form-control" id="emptype">
								@foreach($employeeType as $value)
								<option value="{{$value['id']}}">{{$value['name']}}</option>
								@endforeach
					      </select>
						</div>
						<div class="form-group col-lg-6 col-md-12">
						  <label for="address1">Address 1:</label><i class="material-icons auto-copy" title="Auto fill Address2">navigate_next</i>
						  <textarea class="form-control" rows="5" id="address1"></textarea>
						</div>
						<div class="form-group col-lg-6 col-md-12">
						  <label for="address2">Address 2:</label>
						  <textarea class="form-control" rows="5" id="address2"></textarea>
						</div>
						<div class="col-md-3">
							<label for="department">Department</label>
							<select name="department" id="department" class="form-control">
								@foreach($department as $value)
								<option value="{{$value['id']}}">{{$value['name']}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-3">
							<label for="designation">Designation</label>
							<select name="designation" id="designation" class="form-control">
								@foreach($designation as $value)
								<option value="{{$value['id']}}">{{$value['name']}}</option>
								@endforeach
							</select>
						</div>

						<div class="col-md-3">
							<label for="doj">Date of Joining</label>
							<input type="text" id="doj" name="doj" placeholder="Date of Joining" class="form-control" required>
						</div>

						<div class="col-md-3">
							<label for="mobile">Mobile</label>
							<input type="text" id="mobile" name="mobile" placeholder="Mobile" class="form-control" required>
						</div>
						
						
						<div class="col-md-12 text-center">
							<div class="form-group" style="margin-top: 20px;">
								<button type="submit" class="btn btn-primary save-btn">Save</button>

							</div>
						</div>
		    		</form>
	    		</div>
	    		<div class="col-lg-12 p-lr-0">
		    		<h4 class="col-lg-12">Banking & Accounting Information<div class="loader" id="bannk-loder"></div></h4>
		    		<form action="POST" class="col-lg-12 col-md-12 p-lr-0" id="bank-form">
		    			<div class="form-group col-lg-6 col-md-12">
					      <label for="bankname">Bank Name:</label>
					      <input type="text" class="form-control" id="bankname" placeholder="Enter Bank Name" name="bankname">
		    			</div>
		    			<div class="form-group col-lg-6 col-md-12">
					      <label for="acc">Account Number:</label>
					      <input type="text" class="form-control" id="acc" placeholder="Enter Account Number" name="acc">
		    			</div>
						<div class="form-group col-lg-6 col-md-12">
					      <label for="ifsc">ifsc code:</label>
					      <input type="text" class="form-control" id="ifsc" placeholder="Enter ifsc code" name="ifsc">
		    			</div>
		    			<div class="form-group col-lg-6 col-md-12">
					      <label for="bankbranch">Bank Branch:</label>
					      <input type="text" class="form-control" id="bankbranch" placeholder="Enter Bank Branch" name="bankbranch">
		    			</div>
		    			<div class="form-group col-lg-6 col-md-12">
					      <label for="adhar">Adhar No:</label>
					      <input type="text" class="form-control" id="adhar" placeholder="Enter adhar no." name="adhar">
		    			</div>
		    			<div class="form-group col-lg-6 col-md-12">
					      <label for="pan">PAN No:</label>
					      <input type="text" class="form-control" id="pan" placeholder="Enter PAN no." name="pan">
		    			</div>
		    			<div class="form-group col-lg-6 col-md-12">
					      <label for="esi">ESI No:</label>
					      <input type="text" class="form-control" id="esi" placeholder="Enter ESI No." name="esi">
		    			</div>
		    			<div class="form-group col-lg-6 col-md-12">
					      <label for="epf">EPF No:</label>
					      <input type="text" class="form-control" id="epf" placeholder="Enter EPF no." name="epf">
		    			</div>
						<button type="submit" class="btn btn-primary save-btn">Save</button>
		    		</form>
	    		</div>
	    		<div class="col-lg-12 p-lr-0">
		    		<h4 class="col-lg-12">Educational Qualifications<div class="loader"></div></h4>
		    		<div class="col-lg-12">
		    			<form action="POST" class="col-lg-12 p-lr-0" id="edu-form">
			    			<table class="table table-bordered">
			    				<thead>
			    					<tr>
			    						<th>Education</th>
			    						<th>Studied</th>
			    						<th>School/College/Institution Name And Address</th>
			    						<th>Year</th>
			    					</tr>
			    				</thead>
			    				<tbody>
			    					<tr class="edu-tr">
			    						<td>Up to 10 Std</td>
			    						<td><input type="text" name="studi1" id="studi1"></td>
			    						<td><input type="text" name="add1" id="add1"></td>
			    						<td><input type="number" name="year1" id="year1"></td>
			    					</tr>
			    					<tr class="edu-tr">
			    						<td>Up to 12 Std</td>
			    						<td><input type="text" name="studi2" id="studi2"></td>
			    						<td><input type="text" name="add2" id="add2"></td>
			    						<td><input type="number" name="year2" id="year2"></td>
			    					</tr>
			    					<tr class="edu-tr">
			    						<td>Diploma/Degree</td>
			    						<td><input type="text" name="studi3" id="studi3"></td>
			    						<td><input type="text" name="add3" id="add3"></td>
			    						<td><input type="number" name="year3" id="year3"></td>
			    					</tr>
			    					<tr class="edu-tr">
			    						<td>Master Degree</td>
			    						<td><input type="text" name="studi4" id="studi4"></td>
			    						<td><input type="text" name="add4" id="add4"></td>
			    						<td><input type="number" name="year4" id="year4"></td>
			    					</tr>
			    					<tr class="edu-tr">
			    						<td>Others</td>
			    						<td><input type="text" name="studi5" id="studi5"></td>
			    						<td><input type="text" name="add5" id="add5"></td>
			    						<td><input type="number" name="year5" id="year5"></td>
			    					</tr>
			    				</tbody>
			    			</table>
							<button type="submit" class="btn btn-primary save-btn" id="edu-submit">Save</button>
		    			</form>
		    		</div>
	    		</div>
	    		<div class="col-lg-12 p-lr-0">
					<h4 class="col-lg-12 ped">Previous Experience Details<div class="loader"></div></h4>
					<div class="col-lg-12">
						<input type="number" placeholder="Enter Count." id="cont-input" maxlength="1"/>
						<form action="POST" class="col-lg-12 p-lr-0" id="ped-form">
							<table class="table table-bordered" id="ped-table">
								<thead>
									<tr>
										<th>Name & Address of the orgnization worked</th>
										<th>Designation</th>
										<th>Last drawn Salary</th>
										<th>D.O.J</th>
										<th>D.O.L</th>
										<th>Reason For Leaving</th>
									</tr>
								</thead>
								<tbody id="ped-tbody">
									<tr>
										<td style="display: none;"><input type="hidden" name="id0" id="pid0" value="0"></td>
										<td class="td1"><input type="text" name="name0" required></td>
										<td class="td2"><input type="text" name="des0" required></td>
										<td class="td3"><input type="text" name="lds0" required></td>
										<td class="td4"><input type="text" name="doj0" required></td>
										<td class="td5"><input type="text" name="dol0" required></td>
										<td class="td6"><input type="text" name="reason0" required></td>
									</tr>
								</tbody>
							</table>
							<button type="submit" class="btn btn-primary save-btn">Save</button>
						</form>
					</div>
	    		</div>
	    		<div class="col-lg-12 p-lr-0">
					<h4 class="col-lg-12">Marital Status & Family Details<div class="loader"></div></h4>
	    			<form action="POST" class="col-lg-12 p-lr-0" id="family-form">
		    			<div class="form-group col-lg-12 col-sm-12">
		    				<label for="marital">Marital Status:</label>
		    				<select name="marital" class="form-control" id="marital">
		    					<option value="1">Married</option>
		    					<option value="2">Un Married</option>
		    					<option value="3">Widow</option>
		    					<option value="4">Widower</option>
		    					<option value="5">Other</option>
		    				</select>
		    			</div>
	    				<div class="col-lg-12 p-lr-0 family-div">
		    				<div class="form-group col-sm-2">
		    					<select name="relationsnip0" class="form-control">
		    						<option value="father">Father</option>
		    						<option value="mother">Mother</option>
		    						<option value="wife">Wife</option>
		    						<option value="husband">Husband</option>
		    						<option value="brother">Brother</option>
		    						<option value="sister">Sister</option>
		    						<option value="son">Son</option>
		    						<option value="daughter">Daughter</option>
		    					</select>
		    				</div>
		    				<div class="form-group col-sm-3">
		    					<input type="hidden" name="id0" id="fid0" value="0">
								<input type="text" class="form-control" placeholder="Enter Name" name="name0" required>
		    				</div>
		    				<div class="form-group col-sm-2">
								<input type="text" class="form-control" placeholder="Enter Date of Birth" name="dob0" autocomplete="off" required>
		    				</div>
		    				<div class="form-group col-sm-3">
								<input type="number" class="form-control" placeholder="Enter adhar no." name="adhar0" required>
		    				</div>
		    				<div class="form-group col-sm-2">
								<input type="text" class="form-control" placeholder="Status" name="status0" required>
		    				</div>
	    				</div>
						<button type="submit" class="btn btn-primary save-btn">Save</button>
	    			</form>
    				<span id="add-item">Add Item</span>
	    		</div>
	    		<div class="col-lg-12 p-lr-0">
	    			<h4>Reference Data<div class="loader"></div></h4>
	    			<form action="POST" class="col-lg-12 p-lr-0" id="reference-form">
	    				<div class="form-group col-lg-4">
	    					<label for="cname1">Name</label>
	    					<input type="text" class="form-control" id="cname1" placeholder="Enter Name" name="cname1" required>
	    				</div>
	    				<div class="form-group col-lg-4">
	    					<label for="cmobile1">Mobile Number:</label>
	    					<input type="text" class="form-control" id="cmobile1" placeholder="Enter Mobile No." name="cmobile1" required>
	    				</div>
	    				<div class="form-group col-lg-4">
	    					<label for="ccompany1">Company Name:</label>
	    					<input type="text" class="form-control" id="ccompany1" placeholder="Enter Name" name="ccompany1" required>
	    				</div>
						<div class="form-group col-lg-12">
						  <label for="caddress1">Address:</label>
						  <textarea class="form-control" rows="5" id="caddress1" required></textarea>
						</div>
	    				<div class="form-group col-lg-4">
	    					<label for="cname2">Name</label>
	    					<input type="text" class="form-control" id="cname2" placeholder="Enter Name" name="cname2" required>
	    				</div>
	    				<div class="form-group col-lg-4">
	    					<label for="cmobile2">Mobile Number:</label>
	    					<input type="text" class="form-control" id="cmobile2" placeholder="Enter Mobile No." name="cmobile2" required>
	    				</div>
	    				<div class="form-group col-lg-4">
	    					<label for="ccompany2">Company Name:</label>
	    					<input type="text" class="form-control" id="ccompany2" placeholder="Enter Name" name="ccompany2" required>
	    				</div>
						<div class="form-group col-lg-12">
						  <label for="caddress2">Address:</label>
						  <textarea class="form-control" rows="5" id="caddress2" required></textarea>
						</div>
						<button type="submit" class="btn btn-primary save-btn">Save</button>
	    			</form>
	    		</div>
	    		<div class="col-lg-12 p-lr-0">
	    			<h4>Required Documents<div class="loader"></div></h4>
	    			<div class="col-lg-3 col-md-4 col-sm-6">
	    				<div class="col-lg-12 p-lr-0 img-div">
	    					<label for="passport-img">Empolyee Image:</label>
		    				<img class="doc-img" src="img/img_avatar.png" id="passport-img" alt="">
	    				</div>
	    				<div class="col-lg-12 text-center button-div">
	    					<input class="doc-input" type="file" name="passport" id="passport" accept="image/*">
			                <label for="passport">
			                  <a class="button-tag" id="passport-upload"><i class="material-icons">cloud_upload</i></a>
			                </label>
			                <a href="#" class="button-tag" id="passport-download" download><i class="material-icons">cloud_download</i></a>
	    				</div>
	    			</div>
	    			<div class="col-lg-3 col-md-4 col-sm-6">
	    				<div class="col-lg-12 p-lr-0 img-div">
	    					<label for="family-img">Family Image:</label>
		    				<img class="doc-img" src="img/img_avatar.png" id="family-img" alt="">
	    				</div>
	    				<div class="col-lg-12 text-center button-div">
	    					<input class="doc-input" type="file" name="family" id="family" accept="image/*">
			                <label for="family">
			                  <a class="button-tag" id="family-upload"><i class="material-icons">cloud_upload</i></a>
			                </label>
			                <a href="#" class="button-tag" id="family-download" download><i class="material-icons">cloud_download</i></a>
	    				</div>
	    			</div>
	    			<div class="col-lg-3 col-md-4 col-sm-6">
	    				<div class="col-lg-12 p-lr-0 img-div">
	    					<label for="document1-img">Document1 Image:</label>
		    				<img class="doc-img" src="img/img_avatar.png" id="document1-img" alt="">
	    				</div>
	    				<div class="col-lg-12 text-center button-div">
	    					<input class="doc-input" type="file" name="document1" id="document1" accept="image/*">
			                <label for="document1">
			                  <a class="button-tag" id="document1-upload"><i class="material-icons">cloud_upload</i></a>
			                </label>
			                <a href="#" class="button-tag" id="document1-download" download><i class="material-icons">cloud_download</i></a>
	    				</div>
	    			</div>
	    			<div class="col-lg-3 col-md-4 col-sm-6">
	    				<div class="col-lg-12 p-lr-0 img-div">
	    					<label for="document2-img">Document2 Image:</label>
		    				<img class="doc-img" src="img/img_avatar.png" id="document2-img" alt="">
	    				</div>
	    				<div class="col-lg-12 text-center button-div">
	    					<input class="doc-input" type="file" name="document2" id="document2" accept="image/*">
			                <label for="document2">
			                  <a class="button-tag" id="document2-upload"><i class="material-icons">cloud_upload</i></a>
			                </label>
			                <a href="#" class="button-tag" id="document2-download" download><i class="material-icons">cloud_download</i></a>
	    				</div>
	    			</div>
	    			<div class="col-lg-3 col-md-4 col-sm-6">
	    				<div class="col-lg-12 p-lr-0 img-div">
	    					<label for="document3-img">Document3 Image:</label>
		    				<img class="doc-img" src="img/img_avatar.png" id="document3-img" alt="">
	    				</div>
	    				<div class="col-lg-12 text-center button-div">
	    					<input class="doc-input" type="file" name="document3" id="document3" accept="image/*">
			                <label for="document3">
			                  <a class="button-tag" id="document3-upload"><i class="material-icons">cloud_upload</i></a>
			                </label>
			                <a href="#" class="button-tag" id="document3-download" download><i class="material-icons">cloud_download</i></a>
	    				</div>
	    			</div>
	    			<div class="col-lg-3 col-md-4 col-sm-6">
	    				<div class="col-lg-12 p-lr-0 img-div">
	    					<label for="document4-img">Document4 Image:</label>
		    				<img class="doc-img" src="img/img_avatar.png" id="document4-img" alt="">
	    				</div>
	    				<div class="col-lg-12 text-center button-div">
	    					<input class="doc-input" type="file" name="document4" id="document4" accept="image/*">
			                <label for="document4">
			                  <a class="button-tag" id="document4-upload"><i class="material-icons">cloud_upload</i></a>
			                </label>
			                <a href="#" class="button-tag" id="document4-download" download><i class="material-icons">cloud_download</i></a>
	    				</div>
	    			</div>
	    			<div class="col-lg-3 col-md-4 col-sm-6">
	    				<div class="col-lg-12 p-lr-0 img-div">
	    					<label for="document5-img">Document5 Image:</label>
		    				<img class="doc-img" src="img/img_avatar.png" id="document5-img" alt="">
	    				</div>
	    				<div class="col-lg-12 text-center button-div">
	    					<input class="doc-input" type="file" name="document5" id="document5" accept="image/*">
			                <label for="document5">
			                  <a class="button-tag" id="document5-upload"><i class="material-icons">cloud_upload</i></a>
			                </label>
			                <a href="#" class="button-tag" id="document5-download" download><i class="material-icons">cloud_download</i></a>
	    				</div>
	    			</div>
	    			<div class="col-lg-3 col-md-4 col-sm-6">
	    				<div class="col-lg-12 p-lr-0 img-div">
	    					<label for="document6-img">Document6 Image:</label>
		    				<img class="doc-img" src="img/img_avatar.png" id="document6-img" alt="">
	    				</div>
	    				<div class="col-lg-12 text-center button-div">
	    					<input class="doc-input" type="file" name="document6" id="document6" accept="image/*">
			                <label for="document6">
			                  <a class="button-tag" id="document6-upload"><i class="material-icons">cloud_upload</i></a>
			                </label>
			                <a href="#" class="button-tag" id="document6-download" download><i class="material-icons">cloud_download</i></a>
	    				</div>
	    			</div>
	    			<div class="col-lg-3 col-md-4 col-sm-6">
	    				<div class="col-lg-12 p-lr-0 img-div">
	    					<label for="document7-img">Document7 Image:</label>
		    				<img class="doc-img" src="img/img_avatar.png" id="document7-img" alt="">
	    				</div>
	    				<div class="col-lg-12 text-center button-div">
	    					<input class="doc-input" type="file" name="document7" id="document7" accept="image/*">
			                <label for="document7">
			                  <a class="button-tag" id="document7-upload"><i class="material-icons">cloud_upload</i></a>
			                </label>
			                <a href="#" class="button-tag" id="document7-download" download><i class="material-icons">cloud_download</i></a>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    </div>
	</div> 
	<div id="response-modal" class="modal fade">
	  <div class="modal-dialog" id="save-div">
	    <div class="modal-content">
	      <div class="modal-body">
	        <p class="text-center info-text" id="save-p">Your data has been saved Successfully.</p>
	      </div>
	      <button class="btn btn-primary center-block" id="save-btn" data-dismiss="modal">Okey</button>
	    </div>
	  </div>
	</div>
	<script src="js/employee.js?v=0.2"></script>
	<script type="text/javascript" src="js/smartsearch.js?v=0.1"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>
</body>
</html>