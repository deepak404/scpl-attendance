
<!DOCTYPE html>
<html>
<head>
    <title>Confirmation Report</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->


    <style>

        main{
            border: 1px solid black;
        }

        .header:first-child{
            border-top: none!important;
        }

        .header{
            border-top:1px solid black;
            margin-bottom: 0px;
            padding: 10px 0px;
            font-weight: bold;
        }

        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border: 1px solid black;
        }

        .table-bordered>thead>tr>th:first-child{
            border-left: none;
        }

        .table-bordered>thead>tr>th:last-child {
            border-right: none !important;
        }

        body{
            margin: 0px;
        }

        @page { margin: 15px 20px; }



        .dept-name{
            font-weight: bold;
        }

        table{
            border-collapse: collapse;
            width: 100%;
        }

        td,th{
            padding: 2px 4px;
        }
    </style>
</head>
<body>

<main>
    <p class="header" style="text-align: center;">SHAKTI CORDS PVT LTD</p>
    <p class="header" style="text-align: center;">Confirmation Report as on 24/05/2018</p>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="width: 30px;">S.No</th>
                <th style="text-align: center;">Employee Name</th>
                <th style="width: 60px;text-align: center;">Shift</th>
                <th style="width: 60px;text-align: center;">In</th>
                <th style="width: 60px;text-align: center;">Late</th>
                <th style="border-right: none;text-align: center;">Verified</th>
            </tr>
        </thead>
        <tbody>
        <?php $count = 1 ; ?>
            @foreach($attendance as $deptName =>$employees)
                <tr>
                    <td style="border-left: none"></td>
                    <td class="dept-name">{{$deptName}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="border-right: none"></td>
                </tr>
                @foreach($employees as $employee)
                    <tr>
                        <td style="border-left: none">{{$count}}</td>
                        <td>{{$employee['name']}}</td>
                        <td>{{$employee['shift']}}</td>
                        <td>{{$employee['in_time']}}</td>
                        <td>{{$employee['late']}}</td>
                        <td style="border-right: none">{{$employee['remarks']}}</td>
                    </tr>
                    <?php $count++; ?>

                @endforeach

            @endforeach
        </tbody>
    </table>
</main>
</body>
</html>
