
<!DOCTYPE html>
<html>
<head>
    <title>Daily Report</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->

    
    <style>

        main{
            border: 1px solid black;
        }

        .header:first-child{
            border-top: none!important;
        }

        .header{
            border-top:1px solid black;
            margin-bottom: 0px;
            padding: 10px 0px;
            font-weight: bold;
        }

        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border: 1px solid black;
        }

       table{
            border-collapse: collapse;
            width: 90%;
        }

        tr{
            border: 1px solid black;
        }

        td,th{
            padding: 4px;
        }

        .table-bordered>thead>tr>th:first-child{
            border-left: none;
        }

        .table-bordered>thead>tr>th:last-child {
            border-right: none !important;
        }

        body{
            margin: 0px;
            font-size: 14px;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;    


        }

        @page { margin: 15px 10px; }



        .dept-name{
            font-weight: bold;
        }

        #meals-amount-heading{
            width: 10px !important;
        }
    </style>
</head>
<body>

<main>
    <p class="header" style="text-align: center;">SHAKTI CORDS PVT LTD</p>
    <p class="header" style="text-align: center;">Daily Report as on {{date('d/m/Y', strtotime($date))}}</p>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="width: 15px;">Sl</th>
            <th style="text-align: center; width: 100px;">Employee Name</th>
            <th style="text-align: center; width: 22px;">Shift</th>
            <th style="text-align: center; width: 22px;">In</th>
            <th style="text-align: center; width: 22px;">Out</th>
            <th style="text-align: center; width: 22px;">Wkd Hrs.</th>
            <th style="text-align: center; width: 22px;">Ext Hrs.</th>
            <th style="text-align: center;" id="meals-amount-heading">Meal Amt.</th>
            <th style="text-align: center; width: 20px">Late In</th>
            <th style="text-align: center; width: 20px;">Early Out</th>
            <th style="text-align: center; width: 20px;">Statu<br>s</th>
            <th style="border-right: none; text-align: center;">Remark</th>

        </tr>
        </thead>
        <tbody>
        <?php $count = 1 ; ?>
        @foreach($attendance as $deptName =>$employees)
            <tr>
                <td style="width: 15px; border-left: none"></td>
                <td class="dept-name">{{$deptName}}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="width: 15px;"></td>
                <td style="border-right: none"></td>
            </tr>
            @foreach($employees as $employee)
                <tr>
                    <td style="border-left: none;width: 15px;">{{$count}}</td>
                    <td>{{$employee['name']}}</td>
                    <td>{{$employee['shift']}}</td>
                    <td>{{$employee['in_time']}}</td>
                    <td>{{$employee['out_time']}}</td>
                    <td>{{$employee['worked_hours']}}</td>
                    <td>{{$employee['extra_hours']}}</td>
                    <td>{{$employee['meals_amount']}}</td>
                    <td>{{$employee['late_in']}}</td>
                    <td>{{$employee['early_out']}}</td>
                    <td style="width: 15px;">{{$employee['attendance_status']}}</td>
                    <td style="border-right: none">{{$employee['remarks']}}</td>
                </tr>
                <?php $count++; ?>

            @endforeach

        @endforeach
        </tbody>
    </table>
</main>
</body>
</html>
