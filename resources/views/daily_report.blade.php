<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SCPL | Employee</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="{{url('css/global.css?v=0.1')}}">
    <link rel="stylesheet" href="{{url('css/employee.css?v=0.1')}}">
    <link rel="stylesheet" href="{{url('css/jquery-ui.css?v=0.1')}}">
</head>
<body>
<div class="wrapper container">
    <!-- Sidebar -->
@include('layouts/admin')
<!-- Page Content -->
    <main>
        <div class= row>
            <div class="col-md-12">
                <h2>Daily Report</h2>
                <div id="filter-holder">
                    <ul class="company-list list-inline">
                        <li class="{{ ($id == '1') ? 'active-li' : '' }}">
                            <a href="/daily_report_admin/1">SCPL</a>
                        </li>
                        <li class="{{ ($id == '2') ? 'active-li' : '' }}">
                            <a href="/daily_report_admin/2">GD</a>
                        </li>
                        <li class="{{ ($id == '3') ? 'active-li' : '' }}">
                            <a href="/daily_report_admin/3">GI</a>
                        </li>
                    </ul>
                    <button type="button" class="btn btn-primary pull-right">Approve All</button>
                    <input type="text" id="date-selector" name="date-selector" class="form-control" placeholder="Date" data-companyid="{{$id}}" value="{{$date}}" style="float: right;">
                </div>
                    <div class="table-wrapper">
                    <table class="table table-bordered dept-day-2-day-report-table">
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Employee Name</th>
                            <th>Shift</th>
                            <th>In</th>
                            <th>Out</th>
                            <th>Worked Hours</th>
                            <th>Extra Hours</th>
                            <th>Meals Amount</th>
                            <th>Late In</th>
                            <th>Early Out</th>
                            <th>Status</th>
                            <th>Remark</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="daily-tbody">
                            <?php $count = 1; ?>
                            @foreach($data as $entry)
                                @if($entry['verification'] == 2)
                                <tr id="{{$entry['entry_id']}}">
                                @else
                                <tr style="color: #003ebb;" id="{{$entry['entry_id']}}">
                                @endif
                                    <td>{{$count}}.</td>
                                    <td>{{$entry['emp_name']}}</td>
                                    <td>
                                        <select name="shift-name" id="shift-name{{$entry['entry_id']}}">
                                            @foreach($shifts as $value)
                                                @if($value['in_time'] == $entry['shift'])
                                                <option value="{{$value['in_time']}}" selected>{{$value['in_time']}}</option>
                                                @else
                                                <option value="{{$value['in_time']}}">{{$value['in_time']}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>{{date('H:i:s',strtotime($entry['in_time']))}}</td>
                                    <td>{{date('H:i:s',strtotime($entry['out_time']))}}</td>
                                    <td>{{$entry['wrk_hrs']}}</td>
                                    <td><input type="text" id="extra{{$entry['entry_id']}}" name="extra" value="{{$entry['extra']}}"></td>
                                    <td><input type="text" id="meal{{$entry['entry_id']}}" name="meals-amount" value="{{$entry['meal_amount']}}"></td>
                                    <td>{{$entry['late_in']}}</td>
                                    <td>{{$entry['early_out']}}</td>
                                    <td id="">
                                        <select name="attendance" id="attendance{{$entry['entry_id']}}" value="{{$entry['attendance_status']}}">
                                            @if($entry['attendance_status'] == 'P')
                                            <option value="P" selected>P</option>
                                            @else
                                            <option value="P">P</option>
                                            @endif
                                            @if($entry['attendance_status'] == 'D')
                                            <option value="D" selected>D</option>
                                            @else
                                            <option value="D">D</option>
                                            @endif
                                            @if($entry['attendance_status'] == 'H')
                                            <option value="H" selected>H</option>
                                            @else
                                            <option value="H">H</option>
                                            @endif
                                            @if($entry['attendance_status'] == 'L')
                                            <option value="L" selected>L</option>
                                            @else
                                            <option value="L">L</option>
                                            @endif
                                            @if($entry['attendance_status'] == 'A')
                                            <option value="A" selected>A</option>
                                            @else
                                            <option value="A">A</option>
                                            @endif
                                            @if($entry['attendance_status'] == 'O')
                                            <option value="O" selected>O</option>
                                            @else
                                            <option value="O">O</option>
                                            @endif
                                            @if($entry['attendance_status'] == 'V')
                                            <option value="V" selected>V</option>
                                            @else
                                            <option value="V">V</option>
                                            @endif
                                            @if($entry['attendance_status'] == 'W')
                                            <option value="W" selected>W</option>
                                            @else
                                            <option value="W">W</option>
                                            @endif
                                            @if($entry['attendance_status'] == 'X')
                                            <option value="X" selected>X</option>
                                            @else
                                            <option value="X">X</option>
                                            @endif
                                            @if($entry['attendance_status'] == 'Y')
                                            <option value="Y" selected>Y</option>
                                            @else
                                            <option value="Y">Y</option>
                                            @endif
                                            @if($entry['attendance_status'] == 'Z')
                                            <option value="Z" selected>Z</option>
                                            @else
                                            <option value="Z">Z</option>
                                            @endif
                                        </select>
                                    </td>
                                    <td>
                                        <input type="hidden" id="verification{{$entry['entry_id']}}" value="{{$entry['verification']}}">
                                        <input type="text" id="remarks{{$entry['entry_id']}}" placeholder="Remarks" value="{{$entry['remark']}}">
                                    </td>
                                    <td><button type="button" class="btn btn-primary update-btn" data-entryid="{{$entry['entry_id']}}">Update</button>
                                    </td>
                                </tr>
                                <?php $count += 1; ?>
                            @endforeach
                        </tbody>
                    </table>
                    </div>

            </div>
        </div>
    </main>
</div>
<div id="response-modal" class="modal fade">
    <div class="modal-dialog" id="save-div">
        <div class="modal-content">
            <div class="modal-body">
                <p class="text-center info-text" id="save-p">Your data has been saved Successfully.</p>
            </div>
            <button class="btn btn-primary center-block" id="save-btn" data-dismiss="modal">Okay</button>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{url('js/hod/day_to_day_report.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>
</body>
</html>