<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SCPL | Employee</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="css/global.css">
    <link rel="stylesheet" href="css/employee.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
</head>
<style>
    #filter-report-form{
        margin-left: auto;
        margin-right: auto;
        display: block;
        width: 400px;
        padding-top: 20px;
        padding-bottom: 20px;
        box-shadow: 0px 0px 10px #e8e8e8;
        padding-left: 20px;
    }

    #start_date, #end_date{
        width: 30%;
        display: inline-block;
        margin-right: 10px;
    }
</style>
<body>
<div class="wrapper">
    <!-- Sidebar -->
@include('layouts/admin')
<!-- Page Content -->
    <div id="content" class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Report</h2>

                <form name="filter-report-form" id="filter-report-form" action="get_confirmation_report" method="POST" target="_blank">
                    <div class="form-group">
                        <p>Report Type: </p>
                        <select name="report-type" id="report-type" required>
                            <option value="cr">Confirmation Report</option>
                            <option value="dr">Daily Report</option>
                        </select>
                    </div>

                    <div class="form-group" style="display: none;">
                        <p>Date Range : </p>
                        <input type="text" id="start_date" name="start_date" placeholder="Start date" class="form-control" required disabled>
                        <input type="text" id="end_date" name="end_date" class="form-control" placeholder="End Date" required disabled>

                    </div>


                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Get">

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="response-modal" class="modal fade">
    <div class="modal-dialog" id="save-div">
        <div class="modal-content">
            <div class="modal-body">
                <p class="text-center info-text" id="save-p">Your data has been saved Successfully.</p>
            </div>
            <button class="btn btn-primary center-block" id="save-btn" data-dismiss="modal">Okey</button>
        </div>
    </div>
</div>
<script src="js/employee.js?v=0.1"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $(document).ready(function(){
       $('#report-type').on('change',function(){
          console.log($(this).val());

          if($(this).val() == "dr"){
              $(this).closest('form').find('.form-group:nth-child(2)').show();
              $(this).closest('form').find('.form-group:nth-child(2)').find('input').attr('disabled',false);
              $(this).closest('form').attr('action','get_daily_report');
          }else{
              $(this).closest('form').find('.form-group:nth-child(2)').hide();
              $(this).closest('form').find('.form-group:nth-child(2)').find('input').attr('disabled',true);
              $(this).closest('form').attr('action','get_confirmation_report');
          }

           $(this).closest('form').attr('method','POST');
           $(this).closest('form').attr('target','_blank');

       });

       $('#start_date, #end_date').datepicker();
    });
</script>
</body>
</html>