<nav id="sidebar">
    <div class="sidebar-header">
        <img src="{{url('img/logo.png')}}" alt="SCPL">
    </div>

    <ul class="list-unstyled components">
        <li class="{{ (\Request::route()->getName() == 'admin-home') ? 'active' : '' }}">
            <a class="menu" href="/">Home</a>
        </li>
        <li class="{{ (\Request::route()->getName() == 'employee') ? 'active' : '' }}">
            <a class="menu" href="/employee">Employee</a>
        </li>
        <li class="{{ (\Request::route()->getName() == 'daily_report_admin') ? 'active' : '' }}">
            <a class="menu" href="/daily_report_admin">Daily Report</a>
        </li>
        <li class="{{ (\Request::route()->getName() == 'confirmation_report_admin') ? 'active' : '' }}">
            <a class="menu" href="/confirmation_report_admin">Confirmation Report</a>
        </li>
        <li class="{{ (\Request::route()->getName() == 'manual') ? 'active' : '' }}">
            <a class="menu" href="\manual_entry">Manual Entry</a>
        </li>
        <li class="{{ (\Request::route()->getName() == 'report_list') ? 'active' : '' }}">
            <a class="menu" href="/report_list">Generate Report</a>
        </li>
    </ul>
    <ul class="list-unstyled components" style="position: absolute; bottom: 0;">
        <li>
            <a href="/logout">Logout</a>
        </li>
    </ul>
</nav>
