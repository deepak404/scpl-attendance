<nav id="sidebar">
    <div class="sidebar-header">
        <img src="img/logo.png" alt="SCPL">
    </div>

    <ul class="list-unstyled components">

        <!--        --><?php //dd(\Request::route()->getName()); ?>
    <!-- <p>Dummy Heading</p> -->
        <li class="{{ (\Request::route()->getName() == 'hod_confirmation_report') ? 'active' : '' }}">
            <a class="menu" href="/confirmation_report">Confirmation Report</a>
        </li>


        <li class="{{ (\Request::route()->getName() == 'hod_daily_report') ? 'active' : '' }}">
            <a class="menu" href="/daily_report">Daily Report</a>
        </li>
    </ul>

</nav>