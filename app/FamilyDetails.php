<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FamilyDetails extends Model
{
    public function employee(){
        return $this->belongsTo('App\EmployeeDetails','emp_id');
    }
}
