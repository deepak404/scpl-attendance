<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    public function employee(){
        return $this->belongsTo('App\EmployeeDetails','emp_id');
    }
}
