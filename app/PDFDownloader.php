<?php
/**
 * Created by PhpStorm.
 * User: rightfunds
 * Date: 09/08/18
 * Time: 12:12 PM
 */

namespace App;


use Illuminate\Http\Response;

class PDFDownloader
{

    public function download($path, $fileName){

        $file= $path. $fileName;

        $headers = array(
            'Content-Type: application/pdf',
        );

        return response()->download($file);
    }

}