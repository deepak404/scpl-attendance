<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
    public function employee(){
        return $this->belongsTo('App\EmployeeDetails','emp_id');
    }
}
