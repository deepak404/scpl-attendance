<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
	protected $table = 'bank_account_infos';
	
    public function employee(){
        return $this->belongsTo('App\EmployeeDetails','emp_id');
    }
}
