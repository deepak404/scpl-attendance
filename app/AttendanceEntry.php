<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceEntry extends Model
{
    public function employee(){
        return $this->belongsTo('App\EmployeeDetails');
    }
}
