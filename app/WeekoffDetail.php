<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeekoffDetail extends Model
{

    protected $fillable = ['emp_name', 'weekoff_day','from_date'];
    public function employee(){
        return $this->belongsTo('App\EmployeeDetails', 'emp_id');
    }
}
