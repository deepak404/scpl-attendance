<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeDetails extends Model
{

    protected $fillable = ['first_name','last_name','mobile','doj' ,'dob','emp_code','relationship','address1','address2','emp_type','relative_name','dept_id','company_id','designation'];

    public function department(){
        return $this->belongsTo('App\Department','dept_id');
    }


    public function weeklyOff(){
        return $this->hasMany('App\WeekOffDetail', 'emp_id');
    }

    public function company(){
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function bankAccount(){
        return $this->hasOne('App\BankAccount', 'emp_id');
    }

    public function attendance(){
        return $this->hasMany('App\AttendanceEntry', 'emp_id');
    }
}
