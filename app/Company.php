<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public function department(){
        return $this->hasMany('App\Department', 'company_id');
    }

    public function employees(){
        return $this->hasMany('App\EmployeeDetails', 'company_id');
    }

    public function users(){
        return $this->hasMany('App\Company', 'company_id');
    }
}
