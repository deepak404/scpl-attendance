<?php

namespace App\Exports;

use App\EmployeeDetails;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use DB;

class EmployeeExport implements WithMultipleSheets
{
	use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function sheets(): array
    {
        $employees = DB::table('employee_details')
        				->leftJoin('departments','employee_details.dept_id','=','departments.id')
        				->leftJoin('employee_types','employee_details.emp_type','=','employee_types.id')
        				->leftJoin('companies','employee_details.company_id','=','companies.id')
        				->leftJoin('designations','employee_details.designation','=','designations.id')
        				->orderBy('emp_code')
        				->get(['first_name','last_name','emp_code','departments.name AS dept_name','employee_types.name AS emp_type_name','companies.name AS company','designations.name AS designation'])
        				->groupBy('company')
        				->toArray();
        // dd($employees);
        return $employees;
    }
}
