<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\EmployeeDetails;
use App\AttendanceEntry;
use App\ShiftInfo;
use App\WeekoffDetail;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Department;
Use App\Designation;
Use App\EmployeeType;

class TestController extends Controller
{
    public function populateAttendance()
    {

        AttendanceEntry::truncate();
    	$employees = EmployeeDetails::all()->toArray();
    	// dd($employees); 
    	$period = new DatePeriod(
			     new DateTime(Carbon::now()->subMonths(1)->toDateString()),
			     new DateInterval('P1D'),
			     new DateTime(Carbon::now()->addDays(1)->toDateString())
			);
    	foreach ($period as $key => $value) {
		    $date = $value->format('Y-m-d');
    		foreach ($employees as $empKey => $empValue) {
			    $timeArray = $this->inOut($date);
			    $data_input = $this->shiftCal($timeArray['in_time'],$timeArray['out_time']);
				$early = 0;
				if ($data_input['out'] < date('H:i:s',strtotime($timeArray['out_time']))) {
					$early = null;
				}else{
					$early = $data_input['exit'];
				}
				$lateTime = 0;
				if ($data_input['shift'] > date('H:i:s',strtotime($timeArray['in_time']))) {
					$lateTime = null;
				}else{
					$lateTime = $data_input['diff'];
				}
				if ($date == Carbon::now()->toDateString()) {
					$entry = AttendanceEntry::insert(['emp_name'=>$empValue['first_name'],'emp_id'=>$empValue['id'],'shift_timing'=>date('H:i',strtotime($data_input['shift'])),'emp_code'=>$empValue['emp_code'],'in_time'=>$timeArray['in_time'],'late_in'=>$lateTime,'status'=>'0','verification_status'=>'0']);
				}else{
					$entry = AttendanceEntry::insert(['emp_name'=>$empValue['first_name'],'emp_id'=>$empValue['id'],'shift_timing'=>date('H:i',strtotime($data_input['shift'])),'emp_code'=>$empValue['emp_code'],'in_time'=>$timeArray['in_time'],'late_in'=>$lateTime,'out_time'=>$timeArray['out_time'],'early_out'=>$early,'status'=>'1','verification_status'=>'0']);
				}
    		}
		}
	dd('work done!!!');

    }

    public function inOut($date)
    {
    	$work_hrs = rand(5,12);
    	$min_date = $date.'00.00.00';
    	$max_date = $date.'23.59.59';
    	$in_time = $this->rand_date($min_date,$max_date);
    	$out_time = date("Y-m-d H:i:s", strtotime($in_time.'+'.$work_hrs.' hours'));
    	$in_out = ['in_time'=>$in_time,'out_time'=>$out_time];

    	return $in_out;
    }

    function rand_date($min_date, $max_date) {

	    $min_epoch = strtotime($min_date);
	    $max_epoch = strtotime($max_date);

	    $rand_epoch = rand($min_epoch, $max_epoch);

	    return date('Y-m-d H:i:s', $rand_epoch);
	}

	public function shiftCal($in_time, $out_time)
	{
		$shift = ShiftInfo::where('in_time','!=','00:00:00')->get()->toArray();
		$now = date('H.i',strtotime($in_time));
		$nearestShift = [];
		foreach ($shift as $key => $value) {
			$shiftTime = $value['in_time'];
			$shiftOut = $value['out_time'];
			$diff  = date_diff(date_create($shiftTime),date_create($now));
			$min = 0;
			if (strlen($diff->i) == 1) {
				$min = '0'.$diff->i;
			}else{
				$min = $diff->i;
			}
			$nearestShift[$value['id']]['diff'] = $diff->h.'.'.$min;
			$nearestShift[$value['id']]['shift'] = $shiftTime;
			$nearestShift[$value['id']]['out'] = $shiftOut;
		}
        usort($nearestShift, function($a, $b) {
            return $b['diff'] <=> $a['diff'];
        });
		$shiftPicked = $nearestShift[count($nearestShift)-1];

		$diff  = date_diff(date_create($shiftPicked['out']),date_create($out_time));
		$min = 0;
		if (strlen($diff->i) == 1) {
			$min = '0'.$diff->i;
		}else{
			$min = $diff->i;
		}
		$exitData = $diff->h.'.'.$min;
		$shiftPicked['exit'] = $exitData;

		return $shiftPicked;
	}

	public function empUpdate()
	{
		// $employees = EmployeeDetails::all()->toArray();
		// foreach ($employees as $key => $value) {
		// 	$day = rand(0,6);
		// 	$insert = WeekoffDetail::insert(['emp_id'=>$value['id'],'emp_name'=>$value['first_name'],'weekoff_day'=>$day,'from_date'=>date('Y-m-d',strtotime('2018-07-01'))]);
		// }

        $file = fopen('emp.csv', "r");
        $column = fgetcsv($file);
        while (!feof($file)) {
            $rowData[]=fgetcsv($file);
        }
        foreach ($rowData as $key => $value) {
        	// dd(ucfirst(strtolower($value[4])));

        	// dd($value);

        	$department_id = Department::where('company_id',$value[5])
        								->where('name',$value[4])
        								->first()->value('id');
        	$insert = EmployeeDetails::insert(['dept_id'=>$department_id,
        										'company_id'=>$value[5],
        										'first_name'=>$value[0],
        										'last_name'=>$value[1],
        										'emp_type'=>$value[3],
        										'designation'=>$value[6],
        										'emp_code'=>$value[2],
        										'mobile'=>'9999999999',
        										'dob'=>date('Y-m-d'),
        										'doj'=>date('Y-m-d'),
        										'relationship'=>'1',
        										'relative_name'=>'xxx',
        										'address1'=>'xxx',
        										'address2'=>'xxx'
        									]);
            // ItemMaster::where('material',$value[0])->update([
            //     'hsn'=>$value[1],
            //     'sgst'=>$value[2],
            //     'cgst'=>$value[3],
            //     'igst'=>$value[4]
            // ]);
        }

		dd('work done!!!!');
	}


	public function checkAttendance()
	{
		$attendanceEntry = AttendanceEntry::where('in_time','like',date('Y-m-d',strtotime(Carbon::now()->subDays(1))).'%')->get(['emp_id'])->toArray();
		$myArray = [];
		foreach ($attendanceEntry as $key => $value) {
			$myArray[] = $value['emp_id'];
		}
		$employees = EmployeeDetails::whereNotIn('id',$myArray)->get()->toArray();
		foreach ($employees as $key => $value) {
			dd($value);
		}
	}

 	public function groupByEmpType()
	{
        $file = fopen('emp.csv', "r");
        $column = fgetcsv($file);
        while (!feof($file)) {
            $rowData[]=fgetcsv($file);
        }

        foreach ($rowData as $key => $value) {

        	$data = ucfirst(strtolower($value[4]));
        	if ($data == 'Audit') {
        		$data = 'Auditor';
        	}
        	$department_id = Department::where('name',$data)->value('id');
        	EmployeeDetails::where('emp_code',$value[2])->update(['dept_id'=>$department_id]);

        }

		dd('work done!!!!');

	}


	public function updateAttendance()
	{
		$date = Carbon::now()->subDays(1);
		$date = date('Y-m-d',strtotime($date));
		$emp_ids = AttendanceEntry::where('in_time','like',$date.'%')->pluck('emp_id');
		$employees = EmployeeDetails::whereNotIn('id',$emp_ids)->get();
		foreach ($employees as $key => $value) {
			$insert = AttendanceEntry::insert(['emp_id'=>$value['id'],
											   'emp_name'=>$value['first_name'],
											   'emp_code'=>$value['emp_code'],
											   'in_time'=>$date.' 00:00:00',
											   'out_time'=>$date.' 00:00:00',
											   'shift_timing'=>'00:00:00',
											   'status'=>'1',
											   'attendance_status'=>'A',
											   'verification_status'=>'0',
												]);
		}
		dd('work done!!!');
	}

}
