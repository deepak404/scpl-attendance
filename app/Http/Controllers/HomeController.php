<?php

namespace App\Http\Controllers;

use App\Department;
use App\HolidayDetail;
use DateTime;
use Illuminate\Http\Request;
use App\EmployeeDetails;
use App\BankAccount;
use App\Document;
use App\EducationalDetails;
use App\ExperienceDetail;
use App\FamilyDetails;
use App\Reference;
use PDF;
use App\PDFDownloader;
use Carbon\Carbon;
use App\AttendanceEntry;
use App\ShiftInfo;
use App\Http\Controllers\DepartmentController;
use App\Designation;
use App\EmployeeType;
// use Excel;
use App\Exports\EmployeeExport;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {


        //To check the Department Employees Below

//        $department = Department::find(2);
//
//        foreach ($department as $dept){
//            dd($department->employees);
//        }

//        To check the employee's dept below

//        $employee = EmployeeDetails::first();
//        dd($employee->department);


        return view('index');
    }

    public function employee()
    {
        $department = Department::all();
        $employeeType = EmployeeType::all();
        $designation = Designation::all();
        $employees = EmployeeDetails::orderBy('emp_code')->pluck('emp_code')->toArray();
        $arr2 = range(1,max($employees));
        $next = max($employees)+1;
        $missing = array_diff($arr2,$employees);
        $string_version = implode(' | ', $missing);

        return view('employee',['department'=>$department,'designation'=>$designation,'employeeType'=>$employeeType,'missing'=>$string_version,'empCode'=>$next]);
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * Function Usage :
     *
     * addEmployee Functions adds new employee and updates the existing employees as well
     * The if Condition adds the new employee and else condition updates the existing employee.
     *
     * NOTE : We haven't added the Validation because they are not always particular arguments are passed because all the employee
     * details are not known at the updation time.
     *
     */

    public function addEmployee(Request $request)
    {
        $validate = \Validator::make($request->all(), [
              'firstname' => 'required',
              'lastname' => 'required',
              'dob' => 'required',
              'empcode' => 'required',
              'relation' => 'required',
              'rname' => 'required',
              'emptype' => 'required',
              'department' => 'required',
              'doj' => 'required',
              'mobile' => 'required',
              'company' => 'required',
              'address1' => 'required',
              'address2' => 'required',
              'empid' => 'required',
        ])->validate();

        $dateVal = date('Y-m-d',strtotime($request['dob']));

        if ($request['empid'] == 0) {

            $empcode = EmployeeDetails::where('emp_code',$request['empcode'])->get()->toArray();

            if (count($empcode) != 0) {
                return response()->json(['msg'=>'Employee Code Already Taken. Kindly Change the Employee Code','code'=>0]);
            }

            $employeeDetail = EmployeeDetails::create(['first_name'=>
                                                    strtoupper($request['firstname']),
                                                    'last_name'=>strtoupper($request['lastname']),
                                                    'dob'=>$dateVal,
                                                    'emp_code'=>$request['empcode'],
                                                    'relationship'=>$request['relation'],
                                                    'address1'=>$request['address1'],
                                                    'address2'=>$request['address2'],
                                                    'emp_type'=>$request['emptype'],
                                                    'relative_name'=>$request['rname'],
                                                    'dept_id'=>$request['department'],
                                                    'mobile'=>$request['mobile'],
                                                    'doj'=>date('Y-m-d',strtotime($request['doj'])),
                                                    'company_id'=>$request['company'],
                                                    'designation'=>$request['designation']]);
            if ($employeeDetail) {
                return response()->json(['msg'=>'Sucessfully Created.','empid'=>$employeeDetail->id,'code'=>1]);

            }else{

                return response()->json(['msg'=>'Notable to Create.','code'=>0]);

            }

        }else{
            $empcode = EmployeeDetails::where('emp_code',$request['empcode'])->where('id','!=',
                $request['empid'])->get()->toArray();

            if (count($empcode) != 0) {
                return response()->json(['msg'=>'Employee Code Already Take.','code'=>0]);
            }

            $employeeDetail=EmployeeDetails::where('id',$request['empid'])
                                                ->update(['first_name'=>
                                                    strtoupper($request['firstname']),
                                                    'last_name'=>strtoupper($request['lastname']),
                                                    'dob'=>$dateVal,
                                                    'emp_code'=>$request['empcode'],
                                                    'relationship'=>$request['relation'],
                                                    'address1'=>$request['address1'],
                                                    'address2'=>$request['address2'],
                                                    'emp_type'=>$request['emptype'],
                                                    'relative_name'=>$request['rname'],
                                                    'dept_id'=>$request['department'],
                                                    'mobile'=>$request['mobile'],
                                                    'doj'=>date('Y-m-d',strtotime($request['doj'])),
                                                    'company_id'=>$request['company'],
                                                    'designation'=>$request['designation']]);

            if ($employeeDetail) {

                return response()->json(['msg'=>'Sucessfully Updated.','empid'=>$request['empid'],'code'=>1]);

            }else{

                return response()->json(['msg'=>'Notable to update.','code'=>0]);

            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * Function Usage : Adds Bank Details to the employee. Employee Will have only one bank account.
     * Bank Account can also be edited.
     */

    public function addBank(Request $request)
    {

        $empEntry = BankAccount::where('emp_id',$request['empid'])->get()->toArray();

        if (count($empEntry) == 0) {

            $BankAccount = BankAccount::insert(['emp_id'=>$request['empid'],
                                                'bank_name'=>$request['bankname'],
                                                'acc_no'=>$request['acc'],
                                                'ifsc'=>$request['ifsc'],
                                                'branch'=>$request['bankbranch'],
                                                'aadhar'=>$request['adhar'],
                                                'pan'=>$request['pan'],
                                                'esi'=>$request['esi'],
                                                'epf'=>$request['epf']
                                        ]);

            if ($BankAccount) {

                return response()->json(['msg'=>'Bank Account is Successfully Added.','code'=>1]);

            }else{

                return response()->json(['msg'=>'Bank Account Creation Failed. Please refresh the page and try again.','code'=>0]);

            }
        }else{

            $BankAccount = BankAccount::where('emp_id',$request['empid'])
                                            ->update(['bank_name'=>$request['bankname'],
                                                'acc_no'=>$request['acc'],
                                                'ifsc'=>$request['ifsc'],
                                                'branch'=>$request['bankbranch'],
                                                'aadhar'=>$request['adhar'],
                                                'pan'=>$request['pan'],
                                                'esi'=>$request['esi'],
                                                'epf'=>$request['epf']
                                            ]);
            if ($BankAccount) {
                return response()->json(['msg'=>'Bank Account has been Sucessfully Updated','code'=>1]);
            }else{
                return response()->json(['msg'=>'Bank Account Updation Failed. Please Refresh and try again Later','code'=>0]);
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * Function Usage:
     *
     * The function adds and updates the Employee's Educational Qualification.
     * The if Condition adds the new employee Educational Qaulification and else condition updates the existing employee's Educational Details.
     *
     * NOTE : We haven't added the Validation because they are not always particular arguments are passed because all the employee
     * educational details are not known at the updation time.
     *
     */

    public function addEducation(Request $request)
    {
        $eduEntry = EducationalDetails::where('emp_id',$request['empid'])->get()->toArray();

        if (count($eduEntry) == 0) {

            $inputArray = [];
            $count = 0;

            if ($request['studi1']!=null) {
                $inputArray[$count]['emp_id'] = $request['empid'];
                $inputArray[$count]['stage'] = 's1';
                $inputArray[$count]['qualification'] = $request['studi1'];
                $inputArray[$count]['address'] = $request['add1'];
                $inputArray[$count]['year'] = $request['year1'];
                $count += 1;
            }

            if ($request['studi2']!=null) {
                $inputArray[$count]['emp_id'] = $request['empid'];
                $inputArray[$count]['stage'] = 's2';
                $inputArray[$count]['qualification'] = $request['studi2'];
                $inputArray[$count]['address'] = $request['add2'];
                $inputArray[$count]['year'] = $request['year2'];
                $count += 1;
            }

            if ($request['studi3']!=null) {

                $inputArray[$count]['emp_id'] = $request['empid'];
                $inputArray[$count]['stage'] = 's3';
                $inputArray[$count]['qualification'] = $request['studi3'];
                $inputArray[$count]['address'] = $request['add3'];
                $inputArray[$count]['year'] = $request['year3'];
                $count += 1;

            }

            if ($request['studi4']!=null) {

                $inputArray[$count]['emp_id'] = $request['empid'];
                $inputArray[$count]['stage'] = 's4';
                $inputArray[$count]['qualification'] = $request['studi4'];
                $inputArray[$count]['address'] = $request['add4'];
                $inputArray[$count]['year'] = $request['year4'];
                $count += 1;
            }
            if ($request['studi5']!=null) {

                $inputArray[$count]['emp_id'] = $request['empid'];
                $inputArray[$count]['stage'] = 's5';
                $inputArray[$count]['qualification'] = $request['studi5'];
                $inputArray[$count]['address'] = $request['add5'];
                $inputArray[$count]['year'] = $request['year5'];
                $count += 1;

            }

            $inserted = '';

            foreach ($inputArray as $input) {
                $inserted = EducationalDetails::insert($input);
            }

            if ($inserted != 0) {

                return response()->json(['msg'=>'Educational Details is successfully added.','code'=>1]);

            }else {

                return response()->json(['msg'=>'Education Details creation failed. Please refresh the page and try again.','code'=>0]);
            }
        }else{

            $inputArray = [];
            $count = 0;

            if ($request['studi1']!=null) {

                $inputArray[$count]['emp_id'] = $request['empid'];
                $inputArray[$count]['stage'] = 's1';
                $inputArray[$count]['qualification'] = $request['studi1'];
                $inputArray[$count]['address'] = $request['add1'];
                $inputArray[$count]['year'] = $request['year1'];
                $count += 1;
            }

            if ($request['studi2']!=null) {

                $inputArray[$count]['emp_id'] = $request['empid'];
                $inputArray[$count]['stage'] = 's2';
                $inputArray[$count]['qualification'] = $request['studi2'];
                $inputArray[$count]['address'] = $request['add2'];
                $inputArray[$count]['year'] = $request['year2'];
                $count += 1;

            }
            if ($request['studi3']!=null) {

                $inputArray[$count]['emp_id'] = $request['empid'];
                $inputArray[$count]['stage'] = 's3';
                $inputArray[$count]['qualification'] = $request['studi3'];
                $inputArray[$count]['address'] = $request['add3'];
                $inputArray[$count]['year'] = $request['year3'];

                $count += 1;
            }
            if ($request['studi4']!=null) {

                $inputArray[$count]['emp_id'] = $request['empid'];
                $inputArray[$count]['stage'] = 's4';
                $inputArray[$count]['qualification'] = $request['studi4'];
                $inputArray[$count]['address'] = $request['add4'];
                $inputArray[$count]['year'] = $request['year4'];
                $count += 1;
            }
            if ($request['studi5']!=null) {

                $inputArray[$count]['emp_id'] = $request['empid'];
                $inputArray[$count]['stage'] = 's5';
                $inputArray[$count]['qualification'] = $request['studi5'];
                $inputArray[$count]['address'] = $request['add5'];
                $inputArray[$count]['year'] = $request['year5'];
                $count += 1;

            }
            $inserted = '';
            foreach ($inputArray as $input) {

                $stage = EducationalDetails::where('emp_id',$request['empid'])
                                            ->where('stage',$input['stage'])->get()->toArray();

                if (count($stage) == 0) {

                    $inserted = EducationalDetails::insert($input);

                }else{

                    $inserted = EducationalDetails::where('emp_id',$request['empid'])->where('stage',$input['stage'])->update(['qualification'=>$input['qualification'],'address'=>$input['address'],'year'=>$input['year']]);
                }
            }
            if ($inserted != 0) {

                return response()->json(['msg'=>'Educational Details has been Updated Successfully.','code'=>1]);

            }else{

                return response()->json(['msg'=>'Cannot update Educational Details. Please refresh the Page and try again','code'=>0]);

            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * The if Condition adds the employee's Experience Details and else condition updates the existing employee's Experience Details.
     *
     * NOTE : We haven't added the Validation because they are not always particular arguments are passed because all the employee
     * experience details are not known at the updation time.
     *
     *
     */

    public function addPreviousExperienceDetails(Request $request)
    {
        $pedEntry = ExperienceDetail::where('emp_id',$request['empid'])->get()->toArray();

        if (count($pedEntry) == 0) {

            $entId = [];

            for ($i=0; $i < 9 ; $i++) {

                if (!empty($request['name'.$i])) {

                    $inserted = ExperienceDetail::insertGetId(['emp_id'=>$request['empid'],
                                                        'name_and_address'=>$request['name'.$i],
                                                        'designation'=>$request['des'.$i],
                                                        'last_salary'=>$request['lds'.$i],
                                                        'doj'=>date('Y-m-d',strtotime($request['doj'.$i])),
                                                        'dol'=>date('Y-m-d',strtotime($request['dol'.$i])),
                                                        'reason'=>$request['reason'.$i]
                                                    ]);
                    $entId[] = $inserted;

                }
            }
            if ($inserted != 0) {

                return response()->json(['msg'=>'Sucessfully Created','code'=>1,'id'=>$entId]);

            }else{

                return response()->json(['msg'=>'Notable to create','code'=>1]);
            }

        }else{
            $entId = [];
            for ($i=0; $i < 9 ; $i++) {
                if (!empty($request['name'.$i])) {

                    if ($request['id'.$i] == 0) {
                        $inserted = ExperienceDetail::insertGetId(['emp_id'=>$request['empid'],
                                                            'name_and_address'=>$request['name'.$i],
                                                            'designation'=>$request['des'.$i],
                                                            'last_salary'=>$request['lds'.$i],
                                                            'doj'=>date('Y-m-d',strtotime($request['doj'.$i])),
                                                            'dol'=>date('Y-m-d',strtotime($request['dol'.$i])),
                                                            'reason'=>$request['reason'.$i]
                                                    ]);
                        $entId[] = $inserted;

                    }else{

                        $inserted = ExperienceDetail::where('id',$request['id'.$i])
                                                    ->update(['name_and_address'=>$request['name'.$i],
                                                            'designation'=>$request['des'.$i],
                                                            'last_salary'=>$request['lds'.$i],
                                                            'doj'=>date('Y-m-d',strtotime($request['doj'.$i])),
                                                            'dol'=>date('Y-m-d',strtotime($request['dol'.$i])),
                                                            'reason'=>$request['reason'.$i]
                                                    ]);

                        $entId[] = $request['id'.$i];
                    }
                }
            }
            if ($inserted != 0) {

                return response()->json(['msg'=>'Sucessfully Updated','code'=>1,'id'=>$entId]);

            }else{

                return response()->json(['msg'=>'Notable to update','code'=>1]);

            }
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * Function Usage : The function add's and updates the family member details.
     *
     * The if Condition adds the employee's family Details and else condition updates the existing employee's family Details.
     *
     * NOTE : We haven't added the Validation because they are not always particular arguments are passed because all the employee
     * family details are not known at the updation time.
     *
     */


    public function addFamily(Request $request)
    {
            EmployeeDetails::where('id',$request['empid'])->update(['marital_status'=>$request['marital']]);

            $fmyEntry = FamilyDetails::where('emp_id',$request['empid'])->get()->toArray();

            if (count($fmyEntry) == 0) {

                $entId = [];

                for ($i=0; $i < 10; $i++) {

                    if (!empty($request['name'.$i])) {

                        $inserted = FamilyDetails::insertGetId(['emp_id'=>$request['empid'],
                                                                'name'=>$request['name'.$i],
                                                                'relative'=>$request['relationsnip'.$i],
                                                                'dob'=>date('Y-m-d',strtotime($request['dob'.$i])),
                                                                'aadhar'=>$request['adhar'.$i],
                                                                'status'=>$request['status'.$i]
                                                    ]);

                        $entId[]=$inserted;
                    }
                }

                return response()->json(['msg'=>'Employee Family Details has been created successfully.','code'=>1,'id'=>$entId]);

            }else{
                $entId = [];

                for ($i=0; $i < 10; $i++) {

                    if (!empty($request['name'.$i])) {

                        if ($request['id'.$i] == 0) {

                            $inserted = FamilyDetails::insertGetId(['emp_id'=>$request['empid'],
                                                                    'name'=>$request['name'.$i],
                                                                    'relative'=>$request['relationsnip'.$i],
                                                                    'dob'=>date('Y-m-d',strtotime($request['dob'.$i])),
                                                                    'aadhar'=>$request['adhar'.$i],
                                                                    'status'=>$request['status'.$i]]);
                            $entId[]=$inserted;

                        }else{

                            $inserted = FamilyDetails::where('id',$request['id'.$i])
                                                        ->update(['name'=>$request['name'.$i],
                                                                'relative'=>$request['relationsnip'.$i],
                                                                'dob'=>date('Y-m-d',strtotime($request['dob'.$i])),
                                                                'aadhar'=>$request['adhar'.$i],
                                                                'status'=>$request['status'.$i]]);
                            $entId[]=$request['id'.$i];
                        }
                    }
                }
                return response()->json(['msg'=>'Employee Family Details has been created successfully.','code'=>1,'id'=>$entId]);
            }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * Function Usage :
     *
     * The function add's and updates Employee's Reference Details.
     *
     * * The if Condition adds the employee's Reference Details and else condition updates the existing employee's Reference Details.
     *
     * NOTE : We haven't added the Validation because they are not always particular arguments are passed because all the employee
     * reference details are not known at the updation time.
     */


    public function addReference(Request $request)
    {
        $refEntry = Reference::where('emp_id',$request['empid'])->orderBy('id')->get()->toArray();

        if (count($refEntry) == 0) {

            $inserted = Reference::insert(['emp_id'=>$request['empid'],
                                            'name'=>$request['cname1'],
                                            'mobile'=>$request['cmobile1'],
                                            'company'=>$request['ccompany1'],
                                            'address'=>$request['caddress1']
                                        ]);

            $inserted = Reference::insert(['emp_id'=>$request['empid'],
                                            'name'=>$request['cname2'],
                                            'mobile'=>$request['cmobile2'],
                                            'company'=>$request['ccompany2'],
                                            'address'=>$request['caddress2']
                                        ]);
            if ($inserted != 0) {

                return response()->json(['msg'=>'Employee\'s Reference Details has been Sucessfully Created','code'=>1]);

            }else{

                return response()->json(['msg'=>'Employee\'s Reference Details creation Failed. Refresh the page and try again later','code'=>0]);
            }
        }else{
            $count = 0;

            foreach ($refEntry as $entry) {

                $count += 1;
                $inserted = Reference::where('id',$entry['id'])->update(['name'=>$request['cname'.$count],
                                            'mobile'=>$request['cmobile'.$count],
                                            'company'=>$request['ccompany'.$count],
                                            'address'=>$request['caddress'.$count]
                                        ]);
            }
            if ($inserted != 0) {

                return response()->json(['msg'=>'Employee\'s Reference Details has been Sucessfully Created','code'=>1]);

            }else{

                return response()->json(['msg'=>'Employee\'s Reference Details creation Failed. Refresh the page and try again later','code'=>0]);
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     *
     * Function Usage :
     *
     * The function add's and updates Employee's Document Details.
     *
     * * The if Condition adds the employee's Document Details and else condition updates the existing employee's Document Details.
     *
     * NOTE : We haven't added the Validation because they are not always particular arguments are passed because all the employee
     * Document details are not known at the updation time.
     *
     *
     */


    public function addDocument(Request $request)
    {
        $docEntry = Document::where('emp_id',$request['empid'])->get()->toArray();

        $empCode = EmployeeDetails::where('id',$request['empid'])->value('emp_code');

        if (count($docEntry) == 0) {

            $doctype = $request['doctype'];
            $colName = '';

            $docFile = $request->file('file');
            $file_name = $doctype.'-'.$empCode.'.'. $docFile->getClientOriginalExtension();

            $new_file = $request->file('file')->move(base_path() . "/public/documents/", $file_name);

            switch ($doctype) {

                case 'passport':
                    $colName = 'emp_img';
                    break;
                case 'family':
                    $colName = 'family';
                    break;
                case 'document1':
                    $colName = 'doc1';
                    break;
                case 'document2':
                    $colName = 'doc2';
                    break;
                case 'document3':
                    $colName = 'doc3';
                    break;
                case 'document4':
                    $colName = 'doc4';
                    break;
                case 'document4':
                    $colName = 'doc4';
                    break;
                case 'document5':
                    $colName = 'doc5';
                    break;
                case 'document6':
                    $colName = 'doc6';
                    break;
                case 'document7':
                    $colName = 'doc7';
                    break;

            }

            $inserted = Document::insert(['emp_id'=>$request['empid'],$colName=>$file_name]);

            if ($inserted != 0) {

                return response()->json(['msg'=>'Employee\'s Document has be successfully uploaded','code'=>1,'filename'=>$file_name]);

            }else{

                return response()->json(['msg'=>'Employee\'s Document Upload Failed. Kindly Refresh and Try again.','code'=>0]);
            }
        }else{

            $doctype = $request['doctype'];
            $colName = '';
            $docFile = $request->file('file');

            $file_name = $doctype.'-'.$empCode.'.'. $docFile->getClientOriginalExtension();
            if (file_exists("/public/documents/".$file_name)) {

                unlink("/public/documents/".$file_name);

            }
            $new_file = $request->file('file')->move(base_path() . "/public/documents/", $file_name);

            switch ($doctype) {

                case 'passport':
                    $colName = 'emp_img';
                    break;
                case 'family':
                    $colName = 'family';
                    break;
                case 'document1':
                    $colName = 'doc1';
                    break;
                case 'document2':
                    $colName = 'doc2';
                    break;
                case 'document3':
                    $colName = 'doc3';
                    break;
                case 'document4':
                    $colName = 'doc4';
                    break;
                case 'document4':
                    $colName = 'doc4';
                    break;
                case 'document5':
                    $colName = 'doc5';
                    break;
                case 'document6':
                    $colName = 'doc6';
                    break;
                case 'document7':
                    $colName = 'doc7';
                    break;
            }
            $inserted = Document::where('emp_id',$request['empid'])->update([$colName=>$file_name]);

            if ($inserted != 0) {

                return response()->json(['msg'=>'Sucessfully imported.','code'=>1,'filename'=>$file_name]);

            }else{

                return response()->json(['msg'=>'Notable to imported.','code'=>0]);
            }
        }
    }


    /**
     * @param Request $request
     * @return string
     *
     * Function Usage :
     *
     * The function searches the Employee from the Table according to the search query from the user's input.
     */

    public function findEmployee(Request $request)
    {
        $entry = $request['search-input'];

        $entry = EmployeeDetails::where('first_name','LIKE',$entry.'%')->orwhere('emp_code','LIKE',$entry.'%')->distinct()->limit(10)->get();

        return json_encode($entry);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     *
     * Function Usage : The function gets the Employee ID and gets all the information available about the employee.
     *
     */

    public function getEmployee(Request $request)
    {
        $empid = $request['empid'];
        $finalArray = [];

        $employeeDetail = EmployeeDetails::where('id',$empid)->get()->toArray();
        $finalArray['employeeDetail'] = $employeeDetail[0];
        $BankAccount = BankAccount::where('emp_id',$empid)->get()->toArray();

        if (count($BankAccount) > 0) {

            $finalArray['BankAccount'] = $BankAccount[0];
        }else{

            $finalArray['BankAccount'] = '';
        }

        $eductionalDetails = EducationalDetails::where('emp_id',$empid)->get()->toArray();
        $finalArray['eductionalDetails'] = $eductionalDetails;

        $experienceDetail = ExperienceDetail::where('emp_id',$empid)->get()->toArray();
        $finalArray['experienceDetail'] = $experienceDetail;

        $familyDetails = FamilyDetails::where('emp_id',$empid)->get()->toArray();
        $finalArray['familyDetails'] = $familyDetails;

        $reference = Reference::where('emp_id',$empid)->get()->toArray();
        $finalArray['reference'] = $reference;


        $document = Document::where('emp_id',$empid)->get()->toArray();

        if (count($document) > 0) {

            $finalArray['document'] = $document[0];

        }else{

            $finalArray['document'] = '';
        }

        return response()->json($finalArray);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * Function Usage : The function get the Employee Id and deletes all the available information about the particular Employee
     */


    public function deleteEmployee(Request $request)
    {
        $empid = $request['empid'];

        EmployeeDetails::where('id',$empid)->delete();
        BankAccount::where('empid',$empid)->delete();
        EducationalDetails::where('empid',$empid)->delete();
        ExperienceDetail::where('empid',$empid)->delete();
        FamilyDetails::where('empid',$empid)->delete();
        Reference::where('empid',$empid)->delete();
        Document::where('empid',$empid)->delete();

        return response()->json(['msg'=>'Sucessfully deleted','code'=>1]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * Function Usage : The function gets the CSV file with employee Details and inserts them to Employee Detail's Table.
     *
     */


    public function csvUpload(Request $request)
    {
        $file = $request->file('file');
        $status = $file->move(base_path() . '/storage/csv/', $file->getClientOriginalName());

        if ($status) {

            if (($handle = fopen(base_path().'/storage/csv/'.$file->getClientOriginalName(),'r')) !== FALSE){

                $column=fgetcsv($handle);

                while(!feof($handle)){

                     $rowData[]=fgetcsv($handle);

                }

                foreach ($rowData as $key => $value) {
                    if ($value[0] != "") {

                        $inserted_data=array('firstname'=>$value[0],
                                         'lastname'=>$value[1],
                                         'empcode'=>$value[2],
                                         'dob'=>date('Y-m-d',strtotime($value[3])),
                                         'relationship'=>$value[4],
                                         'relationname'=>$value[5],
                                         'address1'=>$value[6],
                                         'address2'=>$value[7],
                                         'emptype'=>$value[8],
                                    );

                        EmployeeDetails::insert($inserted_data);

                    }
            }

            return response()->json(['msg'=>'Employees Detail has been Successfully Uploaded.','code'=>1]);

            }
            else{

                return response()->json(['msg'=>'Employees Details Upload Failed. Please refresh and try Again.', 'code'=>0]);

            }
        }
        else{
            return response()->json(['msg'=>'Employees Details Upload Failed. Please refresh and try Again.','code'=>0]);
        }


    }



    public function getReportDetails(){
        return view('report_filter');
    }


    public function dailyReport(DepartmentController $dept,$id = '1'){
        $today = date('Y-m-d',strtotime(Carbon::now()->subDays(1)));
        $empIds = EmployeeDetails::where('company_id',$id)->pluck('id');
        $attendanceEntry = AttendanceEntry::whereIn('emp_id', $empIds)
                                            ->where('status',1)
                                            ->where('in_time','like',$today.'%')
                                            ->get()
                                            ->toArray();
        $employees = EmployeeDetails::where('company_id',$id)->get();

        $shifts = ShiftInfo::where('company_id',$id)->get()->toArray();
        $finalArray = [];
        foreach ($attendanceEntry as $key => $value) {
            foreach ($employees as $value1) {
                if ($value['emp_id'] == $value1['id']) {
                    $finalArray[$key]['company_id'] = $value1['company_id'];
                    break;
                }
            }
            $finalArray[$key]['entry_id'] = $value['id'];
            $finalArray[$key]['emp_name'] = $value['emp_name'];
            $finalArray[$key]['shift'] = $value['shift_timing'];
            $finalArray[$key]['in_time'] = $value['in_time'];
            $finalArray[$key]['out_time'] = $value['out_time'];
            $finalArray[$key]['late_in'] = $value['late_in'];
            $finalArray[$key]['early_out'] = $value['early_out'];
            $finalArray[$key]['meal_amount'] = $value['meal_amount'];
            $finalArray[$key]['remark'] = $value['remark'];
            $intime = date('Y-m-d',strtotime($value['in_time'])).' '.$value['shift_timing'];
            $workingHrs = $dept->timeDiff($intime,$value['out_time']);
            $finalArray[$key]['wrk_hrs'] = $workingHrs;
            $finalArray[$key]['verification'] = $value['verification_status'];
            if ($value['extra'] == null) {
                if ($workingHrs > 8) {
                    $extra = $dept->timeDiff(date('H:i:s',strtotime($workingHrs)),date('H:i:s',strtotime('08:00:00')));
                    $finalArray[$key]['extra'] = $extra;
                }else{
                    $finalArray[$key]['extra'] = 0;
                }
            }else{
                $finalArray[$key]['extra'] = $value['extra'];
            }

            if ($value['attendance_status'] == null) {
                if ($value['shift_timing'] == '00:00:00') {
                    $finalArray[$key]['attendance_status'] = 'D';
                    AttendanceEntry::where('id',$value['id'])->update(['attendance_status'=>'D']);
                }else{
                    $timeStamp = strtotime($value['in_time']);
                    $day = date('w',$timeStamp);
                    $attendanceStatusUpdate = $dept->attendanceStatus($workingHrs,$day,$value['in_time'],$value['emp_id']);
                    $finalArray[$key]['attendance_status'] = $attendanceStatusUpdate;
                    AttendanceEntry::where('id',$value['id'])->update(['attendance_status'=>$attendanceStatusUpdate]);
                }

            }else{
                $finalArray[$key]['attendance_status'] = $value['attendance_status'];
            }
        }

        // $data = $this->groupBy($finalArray, 'company_id');
        $data = $finalArray;


        return view('daily_report',['date'=>$today,'data'=>$data,'shifts'=>$shifts,'id'=>$id]);
    }

    public function confirmationReport($id = '1'){
        $today = date('Y-m-d',strtotime(Carbon::now()));
        $empIds = EmployeeDetails::where('company_id',$id)->pluck('id');
        $attendanceEntry = AttendanceEntry::whereIn('emp_id', $empIds)
                                            ->where('status',0)
                                            ->where('in_time','like',$today.'%')
                                            ->get();
        $shifts = ShiftInfo::where('company_id',$id)->orderBy('in_time')->get();
        return view('confirmation_report',['shifts'=>$shifts,'attendance'=>$attendanceEntry,'date'=>$today,'id'=>$id]);
    }

    function groupBy($array, $key) {
        $return = array();
        foreach($array as $val) {
            $return[$val[$key]][] = $val;
        }
        return $return;
    }

    public function populateWeeklyOff(){
        $employees = EmployeeDetails::all();
        foreach ($employees as $employee){
            $employee->weeklyOff()->create(
              [
                  'emp_name' => $employee->first_name,
                  'weekoff_day' => rand(0,6),
                  'from_date' => '2018-01-01'
              ]
            );
        }

//        TO Get Week Off of the Employee

       // $employee = EmployeeDetails::first();
        // dd($employee->weeklyOff->first()->weekoff_day);

    }


    public function showReportList(){
        return view('report_list');
    }


    public function generateReport(Request $request){
        $request->validate([
           'report-type' => 'required'
        ]);


       // dd($_POST);

        switch ($request['report-type']){

            /* Confirmation Report */

            case 'cr':
                $response = $this->generateConfirmationReport();


                if($response == true){
                    return response()->download(public_path().'/'.'Confirmation Report - '.date('d-m-Y').'.pdf');

                }else{
                    return "Sorry. Report Cannot Be generate right now. Please Contact the administrator.";
                }
                break;


                /*Attendance Summary One Below*/

            case 'asone':
                $request->validate([
                    'start_date' => 'required',
                    'end_date' => 'required',
                ]);

                if($request['start_date'] > $request['end_date']){
                    return redirect()->back()->withErrors(['Custom Message' => ['Starting Date is greater than the End date']]);
                }


                break;

            case 'dr':
                $request->validate([
                    'date' => 'required',
                ]);

                $response = $this->generateDailyReport($request['date']);

                if($response == true){
                    return response()->download(public_path().'/'.'Daily Report - '.date('Y-m-d', strtotime($request['date'])).'.pdf');

                }else{
                    return "Sorry. Report Cannot Be generate right now. Please Contact the administrator.";
                }
                break;


            case 'ia':
                //Employee Name is Employee ID
                $request->validate([

                    'employee_name' => 'required',
                    'start_date' => 'required',
                    'end_date' => 'required',
                ]);



                $response = $this->generateIndividualAttendanceReport($request['employee_name'], $request['start_date'], $request['end_date']);
                $employee = EmployeeDetails::where('id', $request['employee_name'])->first();

                $employeeName = $employee->first_name.' '.$employee->last_name;

                if($response == true){
                    return response()->download(public_path().'/'.$employeeName.' - '.$request['start_date'].'-'.$request['end_date'].'.pdf');

                }else{
                    return "Sorry. Report Cannot Be generate right now. Please Contact the administrator.";
                }
                break;

        }


    }


    private function generateConfirmationReport(){
        $department = [];
        $attendance = [];

        $employees = EmployeeDetails::all()->where('company_id',\Auth::user()->company_id)->groupBy('dept_id');


        $deptIds = array_keys($employees->toArray());


        foreach ($deptIds as $id){
            $deptName = Department::where('id', $id)->first()->name;
            $department[$deptName] = $employees[$id];
        }


        foreach ($department as $deptName => $employees){
            foreach ($employees as $employee){
                try{
                    $todayEntry = $employee->attendance()->where('in_time', '>=' ,date('Y-m-d'))->where('status', 0)->first();
                    if (!is_null($todayEntry)) {
                        $attendance[$deptName][] = array(
                            'name' => $todayEntry->emp_name,
                            'shift' => date('h:i a', strtotime($todayEntry->shift_timing)),
                            'in_time' => date('h:i a', strtotime($todayEntry->in_time)),
                            'late' => $todayEntry->late_in,
                            'remarks' => $todayEntry->remark,
                        );
                    }
                }catch(\Exception $e){
                    return false;
                }
            }
        }


        try{
            ksort($attendance);

            $pdf = PDF::loadView('reports.confirmation_report', ['attendance'=>$attendance])->setPaper('a4', 'portrait')->setWarnings(false)->save('Confirmation Report - '.date('d-m-Y').'.pdf');


        }catch(\Exception $e){
            return false;
        }

        return true;




    }


    private function generateDailyReport($date){
        $date = date('Y-m-d', strtotime($date));
        // dd($date);
        $reportEntries = [];



        $employees = \Auth::user()->company->employees;
        $deptNames = Department::where('company_id', \Auth::user()->company_id)->pluck('name','id')->toArray();

        foreach ($employees as $employee){
            try{
                $attendance = $employee->attendance()->where('in_time','like', $date.'%')
                    // ->where('verification_status','!=' ,0)
                    ->where('status', 1)
                    ->first();
                if (is_null($attendance)) {
                    $reportEntries[$deptNames[$employee->dept_id]][] = array(
                        'name' => $employee->first_name,
                        'shift' => date('H:i ', strtotime('07:00')),
                        'in_time' => null,
                        'out_time' => null,
                        'worked_hours' => null,
                        'extra_hours' => null,
                        'meals_amount' => null,
                        'late_in' => null,
                        'early_out' => null,
                        'attendance_status' => 'A',
                        'remarks' => null,
                    );
                }else{
                // dd($attendance);
                    $inTime = new DateTime($attendance->in_time);
                    $outTime = new DateTime($attendance->out_time);

                    $reportEntries[$deptNames[$employee->dept_id]][] = array(
                        'name' => $attendance->emp_name,
                        'shift' => date('H:i ', strtotime($attendance->shift_timing)),
                        'in_time' => date('H:i', strtotime($attendance->in_time)),
                        'out_time' => date('H:i', strtotime($attendance->out_time)),
                        'worked_hours' => date_diff($inTime, $outTime)->format('%h.%i'),
                        'extra_hours' => $attendance->extra,
                        'meals_amount' => $attendance->meal_amount,
                        'late_in' => $attendance->late_in,
                        'early_out' => $attendance->early_out,
                        'attendance_status' => $attendance->attendance_status,
                        'remarks' => $attendance->remark,
                    );
                }
            }catch(\Exception $e){
                return false;
            }

        }

        try{
            ksort($reportEntries);
            PDF::loadView('reports.daily_report', ['attendance'=>$reportEntries, 'date'=> $date])->setPaper('a4', 'portrait')->setWarnings(false)->save('Daily Report - '.$date.'.pdf');

        }catch(\Exception $e){
            return false;
        }

        return true;


    }


    private function generateIndividualAttendanceReport($id, $startDate, $endDate){
        $employee = EmployeeDetails::where('id', $id)->first();

        $attendanceEntries = $employee->attendance()
            ->where('in_time', '>=' ,date('Y-m-d', strtotime($startDate)))
            ->where('out_time', '<=' ,date('Y-m-d', strtotime($endDate)))
            ->get();

        foreach ($attendanceEntries as $attendance){
            try{
                $inTime = new DateTime($attendance->in_time);
                $outTime = new DateTime($attendance->out_time);

                $reportEntries[] = array(
                    'date' => date('d-m-Y ', strtotime($attendance->in_time)),
                    'shift' => date('H:i ', strtotime($attendance->shift_timing)),
                    'in_time' => date('H:i', strtotime($attendance->in_time)),
                    'out_time' => date('H:i', strtotime($attendance->out_time)),
                    'worked_hours' => date_diff($inTime, $outTime)->format('%h.%i'),
                    'extra_hours' => $attendance->extra,
//                    'extra_hours' => 03.01,
                    'meals_amount' => $attendance->meal_amount,
                    'late_in' => $attendance->late_in,
                    'early_out' => $attendance->early_out,
                    'attendance_status' => $attendance->attendance_status,
//                    'attendance_status' => 'A',
                    'remarks' => $attendance->remark,
                );
            }catch(\Exception $e){
                return false;
            }
        }

        try{

//            dd($employee);
            $employeeName = $employee->first_name.' '.$employee->last_name;

            PDF::loadView('reports.individual_attendance_report', ['attendance'=>$reportEntries, 'employeeName'=> $employeeName, 'startDate' => $startDate, 'endDate' => $endDate])
                ->setPaper('a4', 'portrait')
                ->setWarnings(false)
                ->save($employeeName.' - '.$startDate.'-'.$endDate.'.pdf');

        }catch(\Exception $e){
            return false;
        }


        return true;


    }


    public function findAllEmployees(Request $request){
        $entry = $request['search-input'];

        $entry = EmployeeDetails::where('first_name','LIKE',$entry.'%')->orwhere('emp_code','LIKE',$entry.'%')->distinct()->limit(10)->get();

        return json_encode($entry);
    }


    public function dummyView(){
        return view('reports.confirmation_report');
    }

    public function manualEntry()
    {
        // dd('welcome to manual
        return view('manual_entry');
    }

    public function getManual(Request $request)
    {
      $employee = EmployeeDetails::where('emp_code', $request['id'])->get()->first();
      $shifts = ShiftInfo::where('company_id', $employee['company_id'])->get(['in_time'])->toArray();
      $entry = AttendanceEntry::where('emp_code',$request['id'])->where('in_time','LIKE',$request['date'].'%')->get();

      if (count($entry) > 1) {
        return response()->json(['code'=>2,'entrys'=>$entry]);
      }

      return response()->json(['code'=>1,'employee'=>$employee,'shifts'=>$shifts,'entry'=>$entry]);
    }

    public function deleteEntry(Request $request)
    {
      AttendanceEntry::where('id', $request['entry_id'])->delete();
      return response()->json(['code'=>1]);
    }

    public function updateManual(Request $request)
    {
      $inTime = date('Y-m-d H:i:s',strtotime($request['in_time']));
      $outTime = null;
      if (!is_null($request['out_time'])) {
        $outTime = date('Y-m-d H:i:s',strtotime($request['out_time']));
      }
      $lateTime = $request['late_in'];
      $early = $request['early_out'];
      $attendance = $request['attendance_status'];
      $status = $request['status'];
      if (empty($request['entry_id'])) {
        $employee = EmployeeDetails::where('id',$request['emp_id'])->get()->first();
        $insert = AttendanceEntry::insert([
                  'emp_name'=>$employee['first_name'],
                  'emp_id'=>$employee['id'],
                  'shift_timing'=>date('H:i',strtotime($request['shift_timing'])),
                  'emp_code'=>$employee['emp_code'],
                  'in_time'=>$inTime,
                  'out_time'=>$outTime,
                  'late_in'=>$lateTime,
                  'early_out'=>$early,
                  'attendanceStatus'=>$attendance,
                  'status'=>$status,
                  'verification_status'=>'3'
                ]);
        if ($insert) {
          return response()->json(['code'=>1]);
        }else{
          return response()->json(['code'=>0]);
        }
      }else{
        $update = AttendanceEntry::where('id', $request['entry_id'])->update([
                    'shift_timing'=>date('H:i',strtotime($request['shift_timing'])),
                    'in_time'=>$inTime,
                    'out_time'=>$outTime,
                    'late_in'=>$lateTime,
                    'early_out'=>$early,
                    'attendance_status'=>$attendance,
                    'status'=>$status,
                    'verification_status'=>'3'
          ]);
          if ($update) {
            return response()->json(['code'=>1]);
          }else{
            return response()->json(['code'=>0]);
          }
      }
    }

}
