<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AttendanceEntry;
use App\EmployeeDetails;
use App\ShiftInfo;
use DateTime;
use App\Http\Controllers\DepartmentController;

class masterController extends Controller
{
    public function index($key)
    {

    	$qs = str_replace('$', '', $key); // get rid of the $
    	$qs = str_replace('*', '', $qs); // get rid of the *

		$submissions = explode(',', $qs); // split the subs


		$SID = ''; // store for sid
		$MID = ''; // store for mid
		$entry = 0;

		for ($i = 0; $i<count($submissions); $i++) {
				$sections = explode('&', $submissions[$i]);
				if($i == 0) {
					$SID = $sections[0];
					$MID = $sections[1];
					$RFID = $sections[2];
					$DOT = $sections[3];
					$dateTime = $this->timeFormater($DOT);
					$this->attendanceUpdate($RFID,$dateTime);
				} else {
					$RFID = $sections[0];
					$DOT = $sections[1];
					$dateTime = $this->timeFormater($DOT);
					$this->attendanceUpdate($RFID,$dateTime);
				}
		}
		return '$RFID=0#';
    }

    public function timeFormater($data)
    {
    	$string = $data;

		$day = substr($string, 0, 2);
		$month = substr($string, 2, 2);
		$year = substr($string, 4, 4);

		$hour = substr($string, 8, 2);
		$min = substr($string, 10, 2);
		$sec = substr($string, 12, 2);

		$dateTime = $year.'-'.$month.'-'.$day.' '.$hour.':'.$min.':'.$sec;

		$time = strtotime($dateTime);

		$newformat = date('Y-m-d H:i:s',$time);

		return $newformat;
	}

	public function shiftPick($dateTime,$company_id)
	{
		$shift = ShiftInfo::where('in_time','!=','00:00:00')
							->where('company_id',$company_id)
							->get()->toArray();
		$now = date('H.i',strtotime($dateTime));
		$nearestShift = [];
		foreach ($shift as $key => $value) {
			$shiftTime = $value['in_time'];
			$diff  = date_diff(date_create($shiftTime),date_create($now));
			$min = 0;
			if (strlen($diff->i) == 1) {
				$min = '0'.$diff->i;
			}else{
				$min = $diff->i;
			}
			$nearestShift[$value['id']]['diff'] = $diff->h.'.'.$min;
			$nearestShift[$value['id']]['shift'] = $shiftTime;
		}
        usort($nearestShift, function($a, $b) {
            return $b['diff'] <=> $a['diff'];
        });
		$shiftPicked = $nearestShift[count($nearestShift)-1];
		return $shiftPicked;
	}

	public function exitTimeing($shift_out,$out_time)
	{
		$diff  = date_diff(date_create($shift_out),date_create($out_time));
		$min = 0;
		if (strlen($diff->i) == 1) {
			$min = '0'.$diff->i;
		}else{
			$min = $diff->i;
		}
		$exitData = $diff->h.'.'.$min;
		return $exitData;
	}

	public function attendanceUpdate($RFID,$dateTime)
	{
		$employee = EmployeeDetails::where('emp_code',$RFID)->get()->first();
		$entryTime = date('Y-m-d H:i',strtotime($dateTime));
		if ($employee != null) {
			if (AttendanceEntry::where('emp_id',$employee['id'])->where('in_time','like',$entryTime.'%')->orWhere('out_time','like',$entryTime.'%')->get()->count() == 0) {
				$exist = AttendanceEntry::where('status','0')->where('emp_code',$RFID)->get()->toArray();
				if (count($exist) > 0) {
					$empShift = ShiftInfo::where('in_time',$exist[0]['shift_timing'])
										->where('company_id',$employee['company_id'])
										->get()
										->first();
					$late = 0;
					if ($empShift['out_time'] < date('H:i:s',strtotime($dateTime))) {
						$late = null;
					}else{
            if ($this->exitTimeing($exist[0]['in_time'],$dateTime) >= 10) {
              $late = null;
            }else {
              $late = $this->exitTimeing($empShift['out_time'],$dateTime);
            }
					}
					AttendanceEntry::where('id',$exist[0]['id'])->update(['out_time'=>$dateTime,'early_out'=>$late,'status'=>'1']);
				}else{
					$shift = $this->shiftPick($dateTime,$employee['company_id']);
					$lateTime = '';
					if ($shift['shift'] > date('H:i:s',strtotime($dateTime))) {
						$lateTime = null;
					}else{
						$lateTime = $shift['diff'];
					}
					AttendanceEntry::insert(['emp_name'=>$employee['first_name'],'emp_id'=>$employee['id'],'shift_timing'=>date('H:i',strtotime($shift['shift'])),'emp_code'=>$RFID,'in_time'=>$dateTime,'late_in'=>$lateTime,'status'=>'0','verification_status'=>'0']);
				}

			}
		}
	}

}
