<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AttendanceEntry;
use App\EmployeeDetails;
use App\ShiftInfo;
use Carbon\Carbon;
use App\HolidayDetail;
use App\WeekoffDetail;
use App\Notification;

class DepartmentController extends Controller
{
    public function getConfirmationReport(){
    	$today = date('Y-m-d',strtotime(Carbon::now()));
    	$department = \Auth::user()->department->employees->toArray();
    	$employees = [];
    	foreach ($department as $key => $value) {
    		$employees[] = $value['id'];
    	}
    	$attendanceEntry = AttendanceEntry::whereIn('emp_id',$employees)
    										->where('status',0)
    										->where('in_time','like',$today.'%')
    										->where('verification_status',0)
    										->get()
    										->toArray();
    	$shifts = ShiftInfo::all()->toArray();

        return view('hod.daily_report',['shifts'=>$shifts,'attendance'=>$attendanceEntry,'date'=>$today]);
    }

    public function approveAll(Request $request)
    {
    	\Validator::make($request->all(), [
              'date' => 'required',
        ])->validate();
        $date = date('Y-m-d',strtotime($request['date']));
        if (\Auth::user()->role == 'admin') {
            $attendanceEntry = AttendanceEntry::where('status',0)
                                        ->where('in_time','like',$date.'%')
                                        ->where('verification_status',0)
                                        ->update(['verification_status'=>1]);
            return response()->json(['code'=>1,'msg'=>'Sucessfully Approved All.']);
        }
    	$department = \Auth::user()->department->employees->toArray();
    	$employees = [];
    	foreach ($department as $key => $value) {
    		$employees[] = $value['id'];
    	}
    	$attendanceEntry = AttendanceEntry::whereIn('emp_id',$employees)
    										->where('status',0)
    										->where('in_time','like',$date.'%')
    										->where('verification_status',0)
    										->update(['verification_status'=>1]);
    	return response()->json(['code'=>1,'msg'=>'Sucessfully Approved All.']);

    }

    public function getByDate(Request $request)
    {
    	\Validator::make($request->all(), [
              'date' => 'required',
        ])->validate();
        $date = date('Y-m-d',strtotime($request['date']));
        $employees = EmployeeDetails::where('company_id',$request['company_id'])->pluck('id');
        $attendanceEntry = [];
        if (\Auth::user()->role == 'admin') {
            $attendanceEntry = AttendanceEntry::whereIn('emp_id',$employees)
                                            ->where('status',0)
                                            ->where('in_time','like',$date.'%')
                                            ->get(['id','emp_name','in_time','late_in','shift_timing','remark'])
                                            ->toArray();
        }else{
            $department = \Auth::user()->department->employees->toArray();
        	$employees = [];
        	foreach ($department as $key => $value) {
        		$employees[] = $value['id'];
        	}
            $attendanceEntry = AttendanceEntry::whereIn('emp_id',$employees)
                                            ->where('status',0)
                                            ->where('in_time','like',$date.'%')
                                            ->where('verification_status',0)
                                            ->get(['id','emp_name','in_time','late_in','shift_timing','remark'])
                                            ->toArray();
        }
    	$shifts = ShiftInfo::where('company_id',$request['company_id'])->get(['in_time'])->toArray();
    	// dd($attendanceEntry);
    	return response()->json(['code'=>1,'attendance'=>$attendanceEntry,'shifts'=>$shifts]);


    }

    public function updateConfirmation(Request $request)
    {
    	\Validator::make($request->all(), [
              'id' => 'required',
              'shift' => 'required',
              'remark' => 'required',
        ])->validate();

        $attendanceEntry = AttendanceEntry::where('id',$request['id'])->get()->first()->toArray();

        $inTime = date('H:i:s',strtotime($attendanceEntry['in_time']));

        if ($attendanceEntry['shift_timing'] == $request['shift']) {
		    	$update = AttendanceEntry::where('id',$request['id'])->update(['late_in'=>null,'remark'=>$request['remark'],'verification_status'=>1]);
		    	if ($update) {
		    		return response()->json(['code'=>1,'msg'=>'Sucessfully updated.']);
		    	}else{
		    		return response()->json(['code'=>0,'msg'=>'Not able to update attendance.']);
		    	}
        }else{
        	if ($request['shift'] < $inTime) {
        		$late = $this->timeDiff($request['shift'],$inTime);
        		$update = AttendanceEntry::where('id',$request['id'])
        									->update(['late_in'=>$late,
        											'remark'=>$request['remark'],
        											'shift_timing'=>$request['shift'],
        											'verification_status'=>1
        										]);
		    	if ($update) {
		    		return response()->json(['code'=>1,'msg'=>'Sucessfully updated.']);
		    	}else{
		    		return response()->json(['code'=>0,'msg'=>'Not able to update attendance.']);
		    	}
        	}else{
        		$update = AttendanceEntry::where('id',$request['id'])
        									->update(['late_in'=>null,
                                                'remark'=>$request['remark'],
        											'shift_timing'=>$request['shift'],
        											'verification_status'=>1
        										]);
		    	if ($update) {
		    		return response()->json(['code'=>1,'msg'=>'Sucessfully updated.']);
		    	}else{
		    		return response()->json(['code'=>0,'msg'=>'Not able to update attendance.']);
		    	}
        	}
        }

    }

    public function getDailyReport(){    	
    	$today = date('Y-m-d',strtotime(Carbon::now()->subDays(1)));
    	$department = \Auth::user()->department->employees->toArray();
    	$employees = [];
    	foreach ($department as $key => $value) {
    		$employees[] = $value['id'];
    	}
    	$attendanceEntry = AttendanceEntry::whereIn('emp_id',$employees)
    										->where('status',1)
    										->where('in_time','like',$today.'%')
    										->get()
    										->toArray();
    	$shifts = ShiftInfo::all()->toArray();
    	$finalArray = [];
    	foreach ($attendanceEntry as $key => $value) {
    		$finalArray[$key]['entry_id'] = $value['id'];
    		$finalArray[$key]['emp_name'] = $value['emp_name'];
    		$finalArray[$key]['shift'] = $value['shift_timing'];
    		$finalArray[$key]['in_time'] = $value['in_time'];
    		$finalArray[$key]['out_time'] = $value['out_time'];
    		$finalArray[$key]['late_in'] = $value['late_in'];
    		$finalArray[$key]['early_out'] = $value['early_out'];
    		$finalArray[$key]['meal_amount'] = $value['meal_amount'];
    		$finalArray[$key]['remark'] = $value['remark'];
            $intime = date('Y-m-d',strtotime($value['in_time'])).' '.$value['shift_timing'];
    		$workingHrs = $this->timeDiff($intime,$value['out_time']);
    		$finalArray[$key]['wrk_hrs'] = $workingHrs;
    		$finalArray[$key]['verification'] = $value['verification_status'];
    		if ($value['extra'] == null) {
    			if ($workingHrs > 8) {
    				$extra = $this->timeDiff(date('H:i:s',strtotime($workingHrs)),date('H:i:s',strtotime('08:00:00')));
	    			$finalArray[$key]['extra'] = $extra;
    			}else{
	    			$finalArray[$key]['extra'] = 0;
    			}
    		}else{
    			$finalArray[$key]['extra'] = $value['extra'];
    		}

    		if ($value['attendance_status'] == null) {
    			if ($value['shift_timing'] == '00:00:00') {
    				$finalArray[$key]['attendance_status'] = 'D';
    			}else{
	    			$timeStamp = strtotime($value['in_time']);
	    			$day = date('w',$timeStamp);
		    		$finalArray[$key]['attendance_status'] = $this->attendanceStatus($workingHrs,$day,$value['in_time'],$value['emp_id']);
    			}

    		}else{
    			$finalArray[$key]['attendance_status'] = $value['attendance_status'];
    		}
    	}
        return view('hod.day_to_day_report',['date'=>$today,'attendance'=>$finalArray,'shifts'=>$shifts]);
    }

    public function updateDaily(Request $request)
    {
    	\Validator::make($request->all(), [
              'id' => 'required',
              'shift' => 'required',
              'status' => 'required',
              'verification' => 'required',
        ])->validate();
        $attendance = AttendanceEntry::where('id',$request['id'])->get()->first()->toArray();
        if ($request['verification'] == 2) {
        	$description = 'Data';
        	$insert = Notification::insert(['dept_id'=>\Auth::user()->dept_id,'company_id'=>\Auth::user()->company_id,'description'=>$description]);
        }
        if ($attendance['shift_timing'] == $request['shift']) {
        	$update = AttendanceEntry::where('id',$request['id'])
        								->update(['extra'=>$request['extra'],
        										'attendance_status'=>$request['status'],
        										'remark'=>$request['remark'],
        										'meal_amount'=>$request['meal'],
        										'verification_status'=>2]);
        	if ($update) {
        		return response()->json(['code'=>1,'msg'=>'Sucessfully Updated.']);
        	}else{
        		return response()->json(['code'=>0,'msg'=>'Error on Update.']);
        	}
        }elseif ($request['shift'] == '00:00:00') {
        	$update = AttendanceEntry::where('id',$request['id'])
        								->update(['extra'=>$request['extra'],
        										'attendance_status'=>'D',
        										'remark'=>$request['remark'],
        										'meal_amount'=>$request['meal'],
        										'verification_status'=>2,
	        									'shift_timing'=>$request['shift']]);
        	if ($update) {
        		return response()->json(['code'=>1,'msg'=>'Sucessfully Updated.']);
        	}else{
        		return response()->json(['code'=>0,'msg'=>'Error on Update.']);
        	}
        }else{
	        $shift = ShiftInfo::where('in_time',$request['shift'])->get()->first()->toArray();
	    	$dataArray = $this->getLateEraly($shift['in_time'],$shift['out_time'],$attendance['in_time'],$attendance['out_time']);
        	$update = AttendanceEntry::where('id',$request['id'])
        								->update(['extra'=>$request['extra'],
        										'attendance_status'=>$request['status'],
        										'remark'=>$request['remark'],
        										'meal_amount'=>$request['meal'],
        										'verification_status'=>2,
        										'shift_timing'=>$request['shift'],
        										'late_in'=>$dataArray['late'],
        										'early_out'=>$dataArray['early']]);
        	if ($update) {
        		return response()->json(['code'=>1,'msg'=>'Sucessfully Updated.']);
        	}else{
        		return response()->json(['code'=>0,'msg'=>'Error on Update.']);
        	}
        	
        }
    }

    public function timeDiff($time,$time2)
    {
    	if ($time2 == '00:00:00') {
    		$diffTime = null;
    		return $diffTime;
    	}
    	$diff  = date_diff(date_create($time),date_create($time2));
		$min = 0;
		if (strlen($diff->i) == 1) {
			$min = '0'.$diff->i;
		}else{
			$min = $diff->i;
		}
		$diffTime = $diff->h.'.'.$min;
		return $diffTime;
    }

    public function attendanceStatus($hrs,$day,$date,$emp_id)
    {
    	$date1 = date('Y-m-d',strtotime($date)); 
    	$holidays = HolidayDetail::where('date','LIKE',$date1)->get()->toArray();
    	$weekOff = WeekoffDetail::where('emp_id',$emp_id)->where('Weekoff_day',$day)->get()->toArray();
    	$attendanceStatus = '';
    	if (count($holidays) > 0) {
    		if ($hrs < 3.30) {
    			$attendanceStatus = 'X';
    		}elseif ($hrs < 7.30) {
    			$attendanceStatus = 'Z';
    		}else{
    			$attendanceStatus = 'Y';
    		}
    	}elseif (count($weekOff) > 0) {
    		if ($hrs < 3.30) {
    			$attendanceStatus = 'O';
    		}elseif ($hrs < 7.30) {
    			$attendanceStatus = 'V';
    		}else{
    			$attendanceStatus = 'W';
    		}
    	}else{
    		if ($hrs < 3.30) {
    			$attendanceStatus = 'A';
    		}elseif ($hrs < 7.30) {
    			$attendanceStatus = 'H';
    		}else{
    			$attendanceStatus = 'P';
    		}
    	}
    	return $attendanceStatus;
    }

    public function getLateEraly($shiftIn,$shiftOut,$inTime,$outTime)
    {
    	$inTime = date('H:i:s',strtotime($inTime));
    	$outTime = date('H:i:s',strtotime($outTime));
    	$dataArray = [];
    	if ($shiftIn > $inTime) {
    		$dataArray['late'] = null;
    	}else {
    		$dataArray['late'] = $this->timeDiff($shiftIn,$inTime);
    	}

    	if ($shiftOut > $outTime) {
    		$dataArray['early'] = $this->timeDiff($shiftOut, $outTime);
    	}else{
    		$dataArray['early'] = null;
    	}

    	return $dataArray;
    }

    public function getByDay(Request $request)
    {
    	\Validator::make($request->all(), [
              'date' => 'required',
        ])->validate();
    	$date = date('Y-m-d',strtotime($request['date']));
        $employees = EmployeeDetails::where('company_id',$request['company_id'])->pluck('id');
        $attendanceEntry = [];
        if (\Auth::user()->role == 'admin') {
            $attendanceEntry = AttendanceEntry::whereIn('emp_id',$employees)
                                                ->where('status',1)
                                                ->where('in_time','like',$date.'%')
                                                ->get()
                                                ->toArray();
        }else{
    		$department = \Auth::user()->department->employees->toArray();
        	$employees = [];
        	foreach ($department as $key => $value) {
        		$employees[] = $value['id'];
        	}
        	$attendanceEntry = AttendanceEntry::whereIn('emp_id',$employees)
        										->where('status',1)
        										->where('in_time','like',$date.'%')
        										->get()
        										->toArray();
        }
        $employees = EmployeeDetails::all()->toArray();
    	$shifts = ShiftInfo::where('company_id',$request['company_id'])->get()->toArray();
    	$finalArray = [];
    	foreach ($attendanceEntry as $key => $value) {
            foreach ($employees as $value1) {
                if ($value['emp_id'] == $value1['id']) {
                    $finalArray[$key]['company_id'] = $value1['company_id'];
                    break;
                }
            }
    		$finalArray[$key]['entry_id'] = $value['id'];
    		$finalArray[$key]['emp_name'] = $value['emp_name'];
    		$finalArray[$key]['shift'] = $value['shift_timing'];
    		$finalArray[$key]['in_time'] = $value['in_time'];
    		$finalArray[$key]['out_time'] = $value['out_time'];
    		$finalArray[$key]['late_in'] = $value['late_in'];
    		$finalArray[$key]['early_out'] = $value['early_out'];
    		$finalArray[$key]['meal_amount'] = $value['meal_amount'];
    		$finalArray[$key]['remark'] = $value['remark'];
            $intime = date('Y-m-d',strtotime($value['in_time'])).' '.$value['shift_timing'];
    		$workingHrs = $this->timeDiff($intime,$value['out_time']);
    		$finalArray[$key]['wrk_hrs'] = $workingHrs;
    		$finalArray[$key]['verification'] = $value['verification_status'];
    		if ($value['extra'] == null) {
    			if ($workingHrs > 8) {
    				$extra = $this->timeDiff(date('H:i:s',strtotime($workingHrs)),date('H:i:s',strtotime('08:00:00')));
	    			$finalArray[$key]['extra'] = $extra;
    			}else{
	    			$finalArray[$key]['extra'] = 0;
    			}
    		}else{
    			$finalArray[$key]['extra'] = $value['extra'];
    		}

    		if ($value['attendance_status'] == null) {
    			if ($value['shift_timing'] == '00:00:00') {
    				$finalArray[$key]['attendance_status'] = 'D';
    			}else{
	    			$timeStamp = strtotime($value['in_time']);
	    			$day = date('w',$timeStamp);
		    		$finalArray[$key]['attendance_status'] = $this->attendanceStatus($workingHrs,$day,$value['in_time'],$value['emp_id']);
    			}

    		}else{
    			$finalArray[$key]['attendance_status'] = $value['attendance_status'];
    		}
    	}
        $data = $this->groupBy($finalArray, 'company_id');
    	return response()->json(['attendance'=>$finalArray,'shifts'=>$shifts]);
    }

    public function groupBy($array, $key) {
        $return = array();
        foreach($array as $val) {
            $return[$val[$key]][] = $val;
        }
        return $return;
    }

}

