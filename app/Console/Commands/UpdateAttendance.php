<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\DepartmentController;
use App\AttendanceEntry;

class UpdateAttendance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:attendancestatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(DepartmentController $dept)
    {
        $entrys = AttendanceEntry::where('status',1)->where('attendance_status', null)->get();

        foreach ($entrys as $entry) {
          $workingHrs = $dept->timeDiff($entry['in_time'],$entry['out_time']);
          if ($entry['shift_timing'] == '00:00:00') {
              AttendanceEntry::where('id',$entry['id'])->update(['attendance_status'=>'D']);
          }else{
              $timeStamp = strtotime($entry['in_time']);
              $day = date('w',$timeStamp);
              $attendanceStatusUpdate = $dept->attendanceStatus($workingHrs,$day,$entry['in_time'],$entry['emp_id']);
              AttendanceEntry::where('id',$entry['id'])->update(['attendance_status'=>$attendanceStatusUpdate]);
          }

        }
    }
}
