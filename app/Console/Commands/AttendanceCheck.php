<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\EmployeeDetails;
use App\AttendanceEntry;
use Carbon\Carbon;


class AttendanceCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:attendance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check previous day attendance to get attendance flow.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::now();
        $date = date('Y-m-d',strtotime($date));
        $emp_ids = AttendanceEntry::where('in_time','like',$date.'%')->pluck('emp_id');
        $employees = EmployeeDetails::whereNotIn('id',$emp_ids)->get();
        foreach ($employees as $key => $value) {
            $insert = AttendanceEntry::insert(['emp_id'=>$value['id'],
                                               'emp_name'=>$value['first_name'],
                                               'emp_code'=>$value['emp_code'],
                                               'in_time'=>$date.' 00:00:00',
                                               'out_time'=>$date.' 00:00:00',
                                               'shift_timing'=>'00:00:00',
                                               'status'=>'1',
                                               'attendance_status'=>'A',
                                               'verification_status'=>'0',
                                                ]);
        }    
    }
}
