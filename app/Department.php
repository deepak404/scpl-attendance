<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function employees(){
        return $this->hasMany('App\EmployeeDetails', 'dept_id');
    }


    public function user(){
        return $this->hasMany('App\User', 'dept_id');
    }


    public function company(){
        return $this->belongsTo('App\Company', 'company_id');
    }
}
