$(document).ready(function(){ 
	var familyCount = 1;
	var empid = 0;
	var formData = new FormData();
	
	$.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

	$( "#dob, #doj" ).datepicker();

    $('#delete-btn').on('click',function(){
    	if (empid==0) {
			$('#save-p').text('Please Select Employee.');
			$('#response-modal').modal('show');
    	}else{
    		$.ajax({
            type: 'POST',
            url: '/delete_emp',
            data: 'empid='+empid,
            success:function(data){
            	$('#save-p').text(data.message);
        		$('#response-modal').modal('show');
        		location. reload(true);
            },
            error:function(error){ 
				$('#general-loder').css('display','none');
        		$('#save-p').text(error.message);
        		$('#response-modal').modal('show');
            }
        });
    	}
    });
	
	$('.auto-copy').on('click',function(){
		$('#address2').text($('#address1').val());
	});
	
	$('#cont-input').on('keyup',function(){
		$('#ped-tbody').empty();
		console.log($('#cont-input').val());
		for (var i = 0; i < $('#cont-input').val(); i++) {
			$('#ped-tbody').append('<tr>'+
										'<td style="display: none;"><input type="hidden" name="id'+i+'" id="pid'+i+'" value="0"></td>'+
										'<td class="td1"><input type="text" name="name'+i+'" required></td>'+
										'<td class="td2"><input type="text" name="des'+i+'" required></td>'+
										'<td class="td3"><input type="text" name="lds'+i+'" required></td>'+
										'<td class="td4"><input type="text" name="doj'+i+'" required></td>'+
										'<td class="td5"><input type="text" name="dol'+i+'" required></td>'+
										'<td class="td6"><input type="text" name="reason'+i+'" required></td>'+
						'</tr>');
		}
	});
	
	$('#add-item').on('click',function(){
		$('.family-div').append('<div class="form-group col-sm-2">'+
		    					'<select name="relationsnip'+familyCount+'" class="form-control">'+
		    						'<option value="father">Father</option>'+
		    						'<option value="mother">Mother</option>'+
		    						'<option value="wife">Wife</option>'+
		    						'<option value="husband">Husband</option>'+
		    						'<option value="brother">Brother</option>'+
		    						'<option value="sister">Sister</option>'+
		    						'<option value="son">Son</option>'+
		    						'<option value="daughter">Daughter</option>'+
		    					'</select>'+
		    				'</div>'+
		    				'<div class="form-group col-sm-3">'+
			    				'<input type="hidden" name="id'+familyCount+'" id="fid'+familyCount+'" value="0">'+
								'<input type="text" class="form-control" placeholder="Enter Name" name="name'+familyCount+'" required>'+
		    				'</div>'+
		    				'<div class="form-group col-sm-2">'+
								'<input type="text" class="form-control" placeholder="Enter Date of Berth" name="dob'+familyCount+'" autocomplete="off" required>'+
		    				'</div>'+
		    				'<div class="form-group col-sm-3">'+
								'<input type="number" class="form-control" placeholder="Enter adhar no." name="adhar'+familyCount+'" required>'+
		    				'</div>'+
		    				'<div class="form-group col-sm-2">'+
								'<input type="text" class="form-control" placeholder="Status" name="status'+familyCount+'" required>'+
		    				'</div>');
		familyCount += 1;
	});

	$('#empInfo').on('submit',function(e){
		e.preventDefault();
		$('#general-loder').css('display','inline-block');
		var generlData = $('#empInfo').serialize()+'&address1='+$('#address1').val()+'&address2='+$('#address2').val()+'&empid='+empid;
		// console.log(generlData);
		$.ajax({
            type: 'POST',
            url: '/add_emp',
            data: generlData,
            success:function(data){
			$('#general-loder').css('display','none');
            	if (data.code==0) {
	        		$('#save-p').css({color:'#f44336'});
            		$('#save-p').text(data.msg);
            		$('#response-modal').modal('show');
            	}else if (data.code==1) {
            		$('#save-p').css({color:'#59d059'});
            		empid=data.empid;
            		$('#save-p').text(data.msg);
            		$('#response-modal').modal('show');
            	}
            },
            error:function(xhr){
        		$('#save-p').css({color:'#f44336'});
				$('#general-loder').css('display','none');
        		$('#save-p').text(xhr.statusText);
        		$('#response-modal').modal('show');
            }
        });
	});
	
	$('#bank-form').on('submit',function(e){
		e.preventDefault();
		if (empid == 0) {
    		$('#save-p').css({color:'#f44336'});
    		$('#save-p').text('Please Enter General Info.');
    		$('#response-modal').modal('show');
		}else{
			$('#bank-loder').css('display','inline-block');
			var bankData = $('#bank-form').serialize()+'&empid='+empid;
			$.ajax({
	            type: 'POST',
	            url: '/add_bank',
	            data: bankData,
	            success:function(data){
				$('#bank-loder').css('display','none');
	            	if (data.code==0) {
		        		$('#save-p').css({color:'#f44336'});
	            		$('#save-p').text(data.msg);
	            		$('#response-modal').modal('show');
	            	}else if (data.code==1) {
	            		$('#save-p').css({color:'#59d059'});
	            		$('#save-p').text(data.msg);
	            		$('#response-modal').modal('show');
	            	}
	            },
	            error:function(xhr){
	        		$('#save-p').css({color:'#f44336'});
					$('#general-loder').css('display','none');
	        		$('#save-p').text(xhr.statusText);
	        		$('#response-modal').modal('show');
	            }
	        });
		}
	});
	
	$('#edu-form').on('submit',function(e){
		e.preventDefault();
		if (empid == 0) {
    		$('#save-p').css({color:'#f44336'});
    		$('#save-p').text('Please Enter General Info.');
    		$('#response-modal').modal('show');
		}else{
			var eduData = $('#edu-form').serialize()+'&empid='+empid;
			$.ajax({
	            type: 'POST',
	            url: '/add_edu',
	            data: eduData,
	            success:function(data){
				$('#bank-loder').css('display','none');
	            	if (data.code==0) {
		        		$('#save-p').css({color:'#f44336'});
	            		$('#save-p').text(data.msg);
	            		$('#response-modal').modal('show');
	            	}else if (data.code==1) {
	            		$('#save-p').css({color:'#59d059'});
	            		$('#save-p').text(data.msg);
	            		$('#response-modal').modal('show');
	            	}
	            },
	            error:function(xhr){
	        		$('#save-p').css({color:'#f44336'});
					$('#general-loder').css('display','none');
	        		$('#save-p').text(xhr.statusText);
	        		$('#response-modal').modal('show');
	            }
	        });
		}
	});
	$('#ped-form').on('submit',function(e){
		e.preventDefault();
		console.log($('#ped-form').serialize());
		if (empid == 0) {
    		$('#save-p').css({color:'#f44336'});
    		$('#save-p').text('Please Enter General Info.');
    		$('#response-modal').modal('show');
		}else{
			var pedData = $('#ped-form').serialize()+'&empid='+empid;
			$.ajax({
	            type: 'POST',
	            url: '/add_ped',
	            data: pedData,
	            success:function(data){
				$('#bank-loder').css('display','none');
	            	if (data.code==0) {
		        		$('#save-p').css({color:'#f44336'});
	            		$('#save-p').text(data.msg);
	            		$('#response-modal').modal('show');
	            	}else if (data.code==1) {
	            		$('#save-p').css({color:'#59d059'});
	            		var count = 0;
	            		$.each( data.id, function( key, value ) {
						  $('#pid'+count).val(value);
						  count += 1;
						});
	            		$('#save-p').text(data.msg);
	            		$('#response-modal').modal('show');
	            	}
	            },
	            error:function(xhr){
	        		$('#save-p').css({color:'#f44336'});
					$('#general-loder').css('display','none');
	        		$('#save-p').text(xhr.statusText);
	        		$('#response-modal').modal('show');
	            }
	        });
		}
	});

	$('#family-form').on('submit',function(e){
		e.preventDefault();
		if (empid == 0) {
    		$('#save-p').css({color:'#f44336'});
    		$('#save-p').text('Please Enter General Info.');
    		$('#response-modal').modal('show');
		}else{
			var familyData = $('#family-form').serialize()+'&empid='+empid;
			$.ajax({
	            type: 'POST',
	            url: '/add_family',
	            data: familyData,
	            success:function(data){
				$('#bank-loder').css('display','none');
	            	if (data.code==0) {
		        		$('#save-p').css({color:'#f44336'});
	            		$('#save-p').text(data.msg);
	            		$('#response-modal').modal('show');
	            	}else if (data.code==1) {
	            		$('#save-p').css({color:'#59d059'});
	            		var count = 0;
	            		$.each( data.id, function( key, value ) {
						  $('#fid'+count).val(value);
						  count += 1;
						});
	            		$('#save-p').text(data.msg);
	            		$('#response-modal').modal('show');
	            	}
	            },
	           error:function(xhr){
	        		$('#save-p').css({color:'#f44336'});
					$('#general-loder').css('display','none');
	        		$('#save-p').text(xhr.statusText);
	        		$('#response-modal').modal('show');
	            }
	        });
		}
	});

	$('#reference-form').on('submit',function(e){
		e.preventDefault();
		if (empid == 0) {
    		$('#save-p').css({color:'#f44336'});
    		$('#save-p').text('Please Enter General Info.');
    		$('#response-modal').modal('show');
		}else{
			var referenceData = $('#reference-form').serialize()+'&caddress1='+$('#caddress1').val()+'&caddress2='+$('#caddress2').val()+'&empid='+empid;
			$.ajax({
	            type: 'POST',
	            url: '/add_reference',
	            data: referenceData,
	            success:function(data){
				$('#bank-loder').css('display','none');
	            	if (data.code==0) {
		        		$('#save-p').css({color:'#f44336'});
	            		$('#save-p').text(data.msg);
	            		$('#response-modal').modal('show');
	            	}else if (data.code==1) {
	            		$('#save-p').css({color:'#59d059'});
	            		$('#save-p').text(data.msg);
	            		$('#response-modal').modal('show');
	            	}
	            },
	           error:function(xhr){
	        		$('#save-p').css({color:'#f44336'});
					$('#general-loder').css('display','none');
	        		$('#save-p').text(xhr.statusText);
	        		$('#response-modal').modal('show');
	            }
	        });
		}
	});

	$('.doc-input').on('change',function(){
		if (empid == 0) {
    		$('#save-p').css({color:'#f44336'});
    		$('#save-p').text('Please Enter General Info.');
    		$('#response-modal').modal('show');
		}else{
			var file = $(this).get(0).files[0];
			var ext = $(this).val().split('.').pop();
			ext = ext.toLowerCase();
			var format = ['jpg','jpeg','png'];
			var doc_id = $(this).attr('id');
			if ($.inArray(ext, format) !== -1) {
			  formData.delete('file');
			  formData.delete('doctype');
			  formData.delete('empid');
			  formData.append('file',file);
			  formData.append('empid',empid);
			  formData.append('doctype',doc_id);
			  $.ajax({
	            type: 'POST',
	            url: '/add_document',
	            data: formData,
	            contentType: false,
	            processData: false,
	            success:function(data){
				$('#bank-loder').css('display','none');
	            	if (data.code==0) {
		        		$('#save-p').css({color:'#f44336'});
	            		$('#save-p').text(data.msg);
	            		$('#response-modal').modal('show');
	            	}else if (data.code==1) {
	            		$('#save-p').css({color:'#59d059'});
	            		$('#save-p').text(data.msg);
	            		$('#response-modal').modal('show');
	            		$('#'+doc_id+'-img').attr("src","../documents/"+data.filename);
	            		$('#'+doc_id+'-download').attr("href","../documents/"+data.filename);
	            	}
	            },
	           error:function(xhr){
	        		$('#save-p').css({color:'#f44336'});
					$('#general-loder').css('display','none');
	        		$('#save-p').text(xhr.statusText);
	        		$('#response-modal').modal('show');
	            }
	        });
			}else{
	    		$('#save-p').text('Use jpg,jpeg and png For document.');
	    		$('#response-modal').modal('show');
			}
		}
	});

	$(document).on('click', ".suggested-user", function(){
		empid = $(this).data('id');
		$.ajax({
	            type: 'POST',
	            url: '/get_emp',
	            data: 'empid='+empid,
	            success:function(data){
	            	var employeeDetail = data.employeeDetail;
	            	$('#firstname').val(employeeDetail.first_name);
	            	$('#lastname').val(employeeDetail.last_name);;
	            	$('#dob').val(employeeDetail.dob);
	            	$('#empcode').val(employeeDetail.emp_code);
	            	$('#relation').val(employeeDetail.relationship);
	            	$('#rname').val(employeeDetail.relative_name);
	            	$('#emptype').val(employeeDetail.emp_type);
	            	$('#department').val(employeeDetail.dept_id);
	            	$('#designation').val(employeeDetail.designation);
	            	$('#address1').text(employeeDetail.address1);
	            	$('#address2').text(employeeDetail.address2);
	            	$('#doj').val(employeeDetail.doj);
	            	$('#mobile').val(employeeDetail.mobile);
	            	$('#company-id').val(employeeDetail.company_id);
	            	var accountInfo = data.BankAccount;
	            	$('#bankname').val(accountInfo.bank_name);
	            	$('#acc').val(accountInfo.acc_no);
	            	$('#ifsc').val(accountInfo.ifsc);
	            	$('#bankbranch').val(accountInfo.branch);
	            	$('#adhar').val(accountInfo.aadhar);
	            	$('#pan').val(accountInfo.pan);
	            	$('#esi').val(accountInfo.esi);
	            	$('#epf').val(accountInfo.epf);
	            	var eductionalDetails = data.eductionalDetails;
	            	$.each( eductionalDetails, function( key, value ) {
	            		console.log(value.stage);
					  if (value.stage == "s1") {
		            	$("#studi1").val(value.qualification);
		            	$("#add1").val(value.address);
		            	$("#year1").val(value.year);
					  }else if (value.stage == "s2") {
		            	$("#studi2").val(value.qualification);
		            	$("#add2").val(value.address);
		            	$("#year2").val(value.year);
					  }else if (value.stage == "s3") {
		            	$("#studi3").val(value.qualification);
		            	$("#add3").val(value.address);
		            	$("#year3").val(value.year);
					  }else if (value.stage == "s4") {
		            	$("#studi4").val(value.qualification);
		            	$("#add4").val(value.address);
		            	$("#year4").val(value.year);
					  }else if (value.stage == "s5") {
		            	$("#studi5").val(value.qualification);
		            	$("#add5").val(value.address);
		            	$("#year5").val(value.year);
					  }
					});
					var familyDetails = data.familyDetails;
					$('#marital').val(employeeDetail.marital_status);
					$('.family-div').empty();
					familyCount = 0;
	            	$.each( familyDetails, function( key, value ) {
						$('.family-div').append('<div class="form-group col-sm-2">'+
		    					'<select name="relationsnip'+familyCount+'" class="form-control">'+
		    						'<option value="father">Father</option>'+
		    						'<option value="mother">Mother</option>'+
		    						'<option value="wife">Wife</option>'+
		    						'<option value="husband">Husband</option>'+
		    						'<option value="brother">Brother</option>'+
		    						'<option value="sister">Sister</option>'+
		    						'<option value="son">Son</option>'+
		    						'<option value="daughter">Daughter</option>'+
		    					'</select>'+
		    				'</div>'+
		    				'<div class="form-group col-sm-3">'+
			    				'<input type="hidden" name="id'+familyCount+'" id="fid'+familyCount+'" value="'+value.id+'">'+
								'<input type="text" class="form-control" placeholder="Enter Name" name="name'+familyCount+'" value="'+value.relative+'" required>'+
		    				'</div>'+
		    				'<div class="form-group col-sm-2">'+
								'<input type="text" class="form-control" placeholder="Enter Date of Berth" name="dob'+familyCount+'" value="'+value.dob+'" autocomplete="off" required>'+
		    				'</div>'+
		    				'<div class="form-group col-sm-3">'+
								'<input type="number" class="form-control" placeholder="Enter adhar no." name="adhar'+familyCount+'" value="'+value.aadhar+'" required>'+
		    				'</div>'+
		    				'<div class="form-group col-sm-2">'+
								'<input type="text" class="form-control" placeholder="Status" name="status'+familyCount+'" value="'+value.status+'" required>'+
		    				'</div>');
	            		familyCount += 1;
					});
					var experienceDetail = data.experienceDetail;
    				$('#ped-tbody').empty();
    				var i = 0;
	            	$.each( experienceDetail, function( key, value ) {
						$('#ped-tbody').append('<tr>'+
										'<td style="display: none;"><input type="hidden" name="id'+i+'" id="pid'+i+'" value="'+value.id+'"></td>'+
										'<td class="td1"><input type="text" name="name'+i+'" value="'+value.name_and_address+'" required></td>'+
										'<td class="td2"><input type="text" name="des'+i+'" value="'+value.designation+'" required></td>'+
										'<td class="td3"><input type="text" name="lds'+i+'" value="'+value.last_salary+'" required></td>'+
										'<td class="td4"><input type="text" name="doj'+i+'" value="'+value.doj+'" required></td>'+
										'<td class="td5"><input type="text" name="dol'+i+'" value="'+value.dol+'" required></td>'+
										'<td class="td6"><input type="text" name="reason'+i+'" value="'+value.reason+'" required></td>'+
						'</tr>');
						i += 1;

					});
					var reference = data.reference;
					if (reference.length > 0) {
						$('#cname1').val(reference[0].name);
						$('#cname2').val(reference[1].name);
						$('#cmobile1').val(reference[0].mobile);
						$('#cmobile2').val(reference[1].mobile);
						$('#ccompany1').val(reference[0].company);
						$('#ccompany2').val(reference[1].company);
						$('#caddress1').text(reference[0].address);
						$('#caddress2').text(reference[1].address);
					}
					var documents = data.document;
					if (documents.emp_img != null) {
	            		$('#passport-img').attr("src","../documents/"+documents.emp_img);
	            		$('#passport-download').attr("href","../documents/"+documents.emp_img);
					}
					if (documents.family != null) {
	            		$('#family-img').attr("src","../documents/"+documents.family);
	            		$('#family-download').attr("href","../documents/"+documents.family);
					}
					if (documents.doc1 != null) {
	            		$('#document1-img').attr("src","../documents/"+documents.doc1);
	            		$('#document1-download').attr("href","../documents/"+documents.doc1);
					}
					if (documents.doc2 != null) {
	            		$('#document2-img').attr("src","../documents/"+documents.doc2);
	            		$('#document2-download').attr("href","../documents/"+documents.doc2);
					}
					if (documents.doc3 != null) {
	            		$('#document3-img').attr("src","../documents/"+documents.doc3);
	            		$('#document3-download').attr("href","../documents/"+documents.doc3);
					}
					if (documents.doc4 != null) {
	            		$('#document4-img').attr("src","../documents/"+documents.doc4);
	            		$('#document4-download').attr("href","../documents/"+documents.doc4);
					}
					if (documents.doc5 != null) {
	            		$('#document5-img').attr("src","../documents/"+documents.doc5);
	            		$('#document5-download').attr("href","../documents/"+documents.doc5);
					}
					if (documents.doc6 != null) {
	            		$('#document6-img').attr("src","../documents/"+documents.doc6);
	            		$('#document6-download').attr("href","../documents/"+documents.doc6);
					}
					if (documents.doc7 != null) {
	            		$('#document7-img').attr("src","../documents/"+documents.doc7);
	            		$('#document7-download').attr("href","../documents/"+documents.doc7);
					}
	            },
	           error:function(xhr){
	        		$('#save-p').css({color:'#f44336'});
					$('#general-loder').css('display','none');
	        		$('#save-p').text(xhr.statusText);
	        		$('#response-modal').modal('show');
	            }
	        });
	});
	
	$('#csv').on('change',function(){
		var file = $(this).get(0).files[0];
		var csvData = new FormData();
	    formData.append('file',file);
	    $.ajax({
	            type: 'POST',
	            url: '/csv_upload',
	            data: formData,
	            contentType: false,
	            processData: false,
	            success:function(data){
					$('#bank-loder').css('display','none');
            		$('#save-p').text(data.msg);
            		$('#response-modal').modal('show');
	            },
	            error:function(error){ 
					$('#bank-loder').css('display','none');
	        		$('#save-p').text(error.message);
            		$('#response-modal').modal('show');           
	            }
	        });

	});
});