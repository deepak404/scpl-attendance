$(document).ready(function(){
    var users=new Bloodhound({
        datumTokenizer:Bloodhound.tokenizers.whitespace('name'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote:{
            url:'/find_employees?search-input=%QUERY%',
            wildcard:'%QUERY%',},
    });

    $('#employee_name').typeahead({hint:!1,highlight:!1,minLength:1,},
        {
            source:users.ttAdapter(),
            name:'usersList',
            source:users,
            displayKey:'name',
            templates:{
                empty:['<div class = "suggestion no-user">No User Found!</div>'],
                header:['<div class="list-group search-results-dropdown">'],
                suggestion:function(data){
                    user_data=data;
                    return '<a data-id="'+user_data.id+'" class="list-group-item suggested-user"> <span class="suggested-name">'+user_data.first_name+'</span><br><span class="suggested-empcode">'+user_data.emp_code+'</span></a>'}
            }
        });
});