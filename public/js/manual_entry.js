$(document).ready(function(){
    $("#select-date").datepicker({
      dateFormat: "yy-mm-dd"
    });

    $('#in-time, #out-time').datetimepicker({
        format:'Y-m-d H:i:00',
    });

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $('#get_emp_form').on('submit',function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
        type: 'POST',
        url: '/get_manual',
        data: formData,
        success:function(data){
          if (data.code == 2) {
            $('#attendance-tbody').empty();
            $(data.entrys).each(function(key,value){
              $('#attendance-tbody').append('<tr>'+
                            '<td>'+value.emp_name+'</td>'+
                            '<td>'+value.in_time+'</td>'+
                            '<td>'+value.out_time+'</td>'+
                            '<td class="last"><i class="material-icons delete-entry" data-id="'+value.id+'">delete</i></td>'+
                          '</tr>');
            });
            $('#attendance-list').modal('show');
          }else if (data.code == 1) {
            $('#emp_attendance_entry').find("input[type=text]").val("");
            var entry = data.entry;
            var emp = data.employee;
            var shifts = data.shifts;
            console.log(emp.id);
            $('#emp-id').val(emp.id);
            $('#emp-name').val(emp.first_name);
            var company;
            if (emp.company_id === 1) {
              company = "SHAKTHI CORDS Pvt Ltd.";
            }else if (emp.company_id === 2) {
              company = "GD";
            }else{
              company = "GI";
            }
            $('#company').val(company);
            $('#shift-timing').empty();
            $(shifts).each(function(key,value){
              $('#shift-timing').append('<option value="'+value.in_time+'">'+value.in_time+'</option>');
            });
            if (entry.length > 0) {
              $('#entry_id').val(entry[0].id);
              $('#in-time').val(entry[0].in_time);
              $('#out-time').val(entry[0].out_time);
              $('#late-in').val(entry[0].late_in);
              $('#early-out').val(entry[0].early_out);
              $('#shift-timing').val(entry[0].shift_timing);
              $('#attendance-status').val(entry[0].attendance_status);
              $('#status').val(entry[0].status);
            }
          }else{
            console.log(data);
          }

        },
        error:function(error){

        }
      });
    });

    $(document).on('click','.delete-entry',function(){
      $(this).parent().parent().remove();
      $.ajax({
        type: 'POST',
        url: '/delete_entry',
        data: 'entry_id='+$(this).data("id"),
        success:function(data){
          alert('deleted.');
        },
        error:function(error){
          alert('error');
        }
      });
    });

    $('#refresh').on('click',function(){
      location.reload();
    });

    $('#emp_attendance_entry').on('submit',function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
        type: 'POST',
        url: '/update_manual',
        data: formData,
        success:function(data){
          alert('Updated.');
          location.reload();
        },
        error:function(error){
          alert('error');
        }
      });
    })
});
