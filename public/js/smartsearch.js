$(document).ready(function(){
	var users=new Bloodhound({
		datumTokenizer:Bloodhound.tokenizers.whitespace('name'),
		queryTokenizer:Bloodhound.tokenizers.whitespace,
		remote:{
			url:'/find?search-input=%QUERY%',
			wildcard:'%QUERY%',},
		});
		var text = '';
	$('#search-id').typeahead({hint:!1,highlight:!1,minLength:1,},
		{
			source:users.ttAdapter(),
			name:'usersList',
			source:users,
			displayKey:'name',
			templates:{
				empty:['<div class = "suggestion no-user">No User Found!</div>'],
				header:['<div class="list-group search-results-dropdown">'],
				suggestion:function(data){
					user_data=data;
					var company_id = user_data.company_id;
					var company = '';

					if (company_id == '1') {
						company = 'SCPL';
					}else if(company_id == '2') {
						company = 'GD';
					}else{
						company = 'GI';
					}
					return '<a data-id="'+user_data.id+'" class="list-group-item suggested-user"><span class="suggested-name">'+user_data.first_name+'</span><br><span class="suggested-empcode">'+user_data.emp_code+'</span><span class="suggested-empcode pull-right">'+company+'</span></a>'}
				}
			});


			$('.tt-input').on('typeahead:selected', function(evt, item) {
				$('#search-id').typeahead('val', item.first_name);
				$('#emp_id').val(item.emp_code);
			})



});
