$(document).ready(function(){
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

    $('#date-selector').datepicker();

    $('#apprive-btn').on('click',function(){
    	$date = $('#date-selector').val();
    	$.ajax({
    		type:'POST',
    		url:'/approve_all',
    		data: 'date='+$date,
    		success:function(data){
				if (data.code == 0) {
	        		$('#save-p').css({color:'#f44336'});
	        		$('#save-p').text(data.msg);
	        		$('#response-modal').modal('show');	
				}else{
	        		$('#save-p').css({color:'#59d059'});
	        		$('#save-p').text(data.msg);
	        		$('#response-modal').modal('show');					
	    			location.reload();
				}
    		},
    		error:function(xhr){
        		$('#save-p').css({color:'#f44336'});
				$('#general-loder').css('display','none');
        		$('#save-p').text(xhr.statusText);
        		$('#response-modal').modal('show');
    		}
    	})
    });

    $('#date-selector').on('change',function(){
    	var date = $(this).val();
        var company_id = $(this).data('companyid');
    	$.ajax({
    		type:'POST',
    		url:'/get_by_date',
    		data:'date='+date+'&company_id='+company_id,
    		success:function(data){
				$('#daily-report-tboby').empty();
				var count = 1;
    			$.each(data.attendance, function(key, value){
    				var in_time = value.in_time;
    				in_time = in_time.split(" ");
    				var late_in = '';
    				if (value.late_in != null) {
    					late_in = value.late_in;
    				}
    				var remark = '';
    				if (value.remark != null) {
    					remark = value.remark;
    				}
    				$('#daily-report-tboby').append('<tr id="'+value.id+'">'+
				                                    '<td>'+count+'.</td>'+
				                                    '<td>'+value.emp_name+'</td>'+
				                                    '<td>'+
			                                        '<select class="shift-name" name="shift-name" id="shift-name'+value.id+'" >'+
			                                        '</select>'+
			                                        '</td>'+
			                                        '<td>'+in_time[1]+'</td>'+
				                                    '<td>'+late_in+'</td>'+
				                                    '<td>'+
			                                        '<input type="text" id="remarks'+value.id+'" value="'+remark+'" placeholder="Remarks">'+
			                                        '<button type="button" class="btn btn-primary update-btn" data-entryid="'+value.id+'">Update</button>'+
				                                    '</td>'+
				                                    '</tr>');
    				$.each(data.shifts, function(key1, value1){
    					  var parent = document.getElementById('shift-name'+value.id);
						  var newChild = '<option value="'+value1.in_time+'">'+value1.in_time+'</option>';
						  parent.insertAdjacentHTML('beforeend', newChild);
    				});
    				$('#shift-name'+count).val(value.shift_timing);
    				count++;
    			});

    		},
    		error:function(xhr){
        		$('#save-p').css({color:'#f44336'});
				$('#general-loder').css('display','none');
        		$('#save-p').text(xhr.statusText);
        		$('#response-modal').modal('show');
    		}
    	});
    });

    $(document).on('click','.update-btn',function(){
		var rowId = $(this).data("entryid");
		var shift = $('#shift-name'+rowId).val();
		var remark = $('#remarks'+rowId).val();
		var formData = "id="+rowId+'&shift='+shift+'&remark='+remark;
		$.ajax({
			type:'POST',
			url:'/update_confirmation',
			data:formData,
			success:function(data){
				if (data.code == 0) {
	        		$('#save-p').css({color:'#f44336'});
	        		$('#save-p').text(data.msg);
	        		$('#response-modal').modal('show');	
				}else{
	        		$('#save-p').css({color:'#59d059'});
	        		$('#save-p').text(data.msg);
	        		$('#response-modal').modal('show');	
	    			location.reload();				
				}

			},
			error:function(xhr){
				// console.log(xhr);
				// console.log(xhr.responseJSON.errors.remark[0]);
	    		$('#save-p').css({color:'#f44336'});
				$('#general-loder').css('display','none');
				if (xhr.status == 422) {
		    		$('#save-p').text(xhr.responseJSON.errors.remark[0]);
				}else{
		    		$('#save-p').text(xhr.statusText);
				}
	    		$('#response-modal').modal('show');
			}
		});
    });

});