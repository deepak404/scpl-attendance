$(document).ready(function(){

    $('#date-selector').datepicker();

	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

    $(document).on('click','.update-btn',function(){
    	var rowId = $(this).data('entryid');
    	var shift = $('#shift-name'+rowId).val();
    	var extra = $('#extra'+rowId).val();
    	var meal = $('#meal'+rowId).val();
    	if (meal == 'null') {
    		meal = '';
    	}
    	var status = $('#attendance'+rowId).val();
    	var remark = $('#remarks'+rowId).val();
    	if (remark == 'null') {
    		remark = '';
    	}
    	var verification = $('#verification'+rowId).val();
    	var formData = 'id='+rowId+'&shift='+shift+'&extra='+extra+'&meal='+meal+'&status='+status+'&remark='+remark+'&verification='+verification;
    	console.log(formData);
    	$.ajax({
    		type:'POST',
    		url:'update_daily',
    		data:formData,
    		success:function(data){
				if (data.code == 0) {
	        		$('#save-p').css({color:'#f44336'});
	        		$('#save-p').text(data.msg);
	        		$('#response-modal').modal('show');	
				}else{
	        		$('#save-p').css({color:'#59d059'});
	        		$('#save-p').text(data.msg);
	        		$('#response-modal').modal('show');					
	    			location.reload();
				}
    		},
    		error:function(xhr){
        		$('#save-p').css({color:'#f44336'});
				$('#general-loder').css('display','none');
        		$('#save-p').text(xhr.statusText);
        		$('#response-modal').modal('show');
    		}
    	})
    });

    $('#date-selector').on('change',function(){
    	var date = $(this).val();
        var company_id = $(this).data('companyid');
    	$.ajax({
    		type:'POST',
    		url:'/get_by_day',
    		data:'date='+date+'&company_id='+company_id,
    		success:function(data){
    			$('#daily-tbody').empty();
    			var count = 1;
    			$.each(data.attendance, function(key,value){
    				var color = '';
                    var meal_amounts = '';
                    var late_in = '';
                    var early_out = '';
                    var remark = '';
                    if (value.meal_amount != null) {
                        meal_amounts = value.meal_amount;
                    }
                    if (value.late_in != null) {
                        late_in = value.late_in;
                    }
                    if (value.early_out != null) {
                        early_out = value.early_out;
                    }
                    if (value.remark != null) {
                        remark = value.remark;
                    }
    				var in_time = value.in_time;
    				in_time = in_time.split(" ");
    				var out_time = value.out_time;
    				out_time = out_time.split(" ");
    				if (value.verification == 2) {
    					color = 'style=""';
    				}else{
    					color = 'style="color: #003ebb;"';
    				}
    				$('#daily-tbody').append('<tr '+color+' id="'+value.entry_id+'">'+
    										'<td>'+count+'.</td>'+
    										'<td>'+value.emp_name+'</td>'+
    										'<td>'+
    										'<select name="shift-name" id="shift-name'+value.entry_id+'">'+
    										'</select>'+
    										'</td>'+
    										'<td>'+in_time[1]+'</td>'+
    										'<td>'+out_time[1]+'</td>'+
    										'<td>'+value.wrk_hrs+'</td>'+
    										'<td><input type="text" id="extra'+value.entry_id+'" name="extra" value="'+value.extra+'"></td>'+
    										'<td><input type="text" id="meal'+value.entry_id+'" name="meals-amount" value="'+meal_amounts+'"></td>'+
    										'<td>'+late_in+'</td>'+
    										'<td>'+early_out+'</td>'+
    										'<td>'+
    										'<select id="attendance'+value.entry_id+'">'+
    										'<option value="P">P</option>'+
                                            '<option value="D">D</option>'+
                                            '<option value="H">H</option>'+
                                            '<option value="L">L</option>'+
                                            '<option value="A">A</option>'+
                                            '<option value="O">O</option>'+
                                            '<option value="V">V</option>'+
                                            '<option value="W">W</option>'+
                                            '<option value="X">X</option>'+
                                            '<option value="Y">Y</option>'+
                                            '<option value="Z">Z</option>'+
                                            '</select>'+
    										'</td>'+
    										'<td>'+
    										'<input type="hidden" id="verification'+value.entry_id+'" value="'+value.verification+'">'+
    										'<input type="text" id="remarks'+value.entry_id+'" placeholder="Remarks" value="'+remark+'">'+
    										'</td>'+
    										'<td><button type="button" class="btn btn-primary update-btn" data-entryid="'+value.entry_id+'">Update</button>'+
                                    		'</td>'+
                                    		'</tr>');
    				$.each(data.shifts, function(key1, value1){
    					  var parent = document.getElementById('shift-name'+value.entry_id);
						  var newChild = '<option value="'+value1.in_time+'">'+value1.in_time+'</option>';
						  parent.insertAdjacentHTML('beforeend', newChild);
    				});
    				$('#shift-name'+value.entry_id).val(value.shift);
    				$('#attendance'+value.entry_id).val(value.attendance_status);
    				count++;
    			});
    		},
    		error:function(xhr){
        		$('#save-p').css({color:'#f44336'});
				$('#general-loder').css('display','none');
        		$('#save-p').text(xhr.statusText);
        		$('#response-modal').modal('show');
    		}
    	})
    });

});